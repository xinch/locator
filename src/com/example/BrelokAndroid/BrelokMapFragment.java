package com.example.BrelokAndroid;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.graphics.Color;
import android.support.v4.app.Fragment;
import android.widget.Button;
import android.widget.CheckBox;
import com.example.BrelokAndroid.BrelokDataStructures.BrelokMsgLocation;
import com.example.BrelokAndroid.DBTables.BrelokCoordinatesArchive;
import com.google.android.gms.maps.*;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;


/**
 * Created with IntelliJ IDEA.
 * User: Admin
 * Date: 23.09.13
 * Time: 15:14
 * To change this template use File | Settings | File Templates.
 */
public class BrelokMapFragment extends Fragment {

    private BrelokDbAdapter dbAdapter;
    private BroadcastReceiver broadcastReceiver;
    private GoogleMap map;
    private CheckBox checkBoxFollow;
    private Cursor cursor;
    View rootView;

    public BrelokMapFragment(){
        super();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState){
        if (rootView != null)
        {
            ViewGroup parent = (ViewGroup) rootView.getParent();
            if (parent != null)
                parent.removeView(rootView);
        }
        rootView = inflater.inflate(R.layout.fragment_map, container, false);
        map = ((SupportMapFragment) getFragmentManager().findFragmentById(R.id.map_fragment)).getMap();
        dbAdapter = new BrelokDbAdapter(getActivity());
        getActivity().getSupportFragmentManager().popBackStack();

        //getFragmentManager().findFragmentById(R.id.map_fragment).getView().setVisibility(View.INVISIBLE);

        Button btnMyLocatin=(Button) rootView.findViewById(R.id.btnMyLocation);
        btnMyLocatin.setOnClickListener(new View.OnClickListener() {// нажатие на кнопку Мое местоположение
            @Override
            public void onClick(View v) {
                addMarker(updateMyloc(),true);
            }
        });

        checkBoxFollow = (CheckBox) rootView.findViewById(R.id.chkBoxFollow);
        return rootView;
    }

    @Override
    public void onPause() {
        super.onPause();
        getActivity().unregisterReceiver(broadcastReceiver);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onResume(){
        super.onResume();
        // подписываемся на широковещательные сообщения
        IntentFilter intentFilter=new IntentFilter("android.intent.action.MAIN");
        broadcastReceiver =new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                if(intent.getIntExtra("task",1)==1){   //пришла комадна обновиться
                    addMarker(updateMyloc(),false);
                }
            }
        };
        getActivity().registerReceiver(this.broadcastReceiver, intentFilter);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState){
        super.onActivityCreated(savedInstanceState);
    }

    /***
     * тянет из базы последние координаты устройства и ставит маркер на карту
     */
    private BrelokMsgLocation updateMyloc(){
        dbAdapter.openHelper();
        cursor = dbAdapter.getLastDeviceCoordinates();
        BrelokMsgLocation myLocation=null;
        if (cursor!=null)
        {
            if (cursor.moveToFirst()){
                myLocation=new BrelokMsgLocation(cursor.getDouble(cursor.getColumnIndex(BrelokCoordinatesArchive.colLatitude)),
                        cursor.getDouble(cursor.getColumnIndex(BrelokCoordinatesArchive.colLongitude)));
                        //cursor.getDouble(cursor.getColumnIndex(BrelokCoordinatesArchive.colAltitude)));
            }
        }
        dbAdapter.closeHelper();
        return myLocation;
    }

    /***
     * метод ставит маркер на карту
     * @param location
     */
    private void addMarker(BrelokMsgLocation location,boolean isZoom) {
        if(location!=null){
            map.clear();
            LatLng curPos=new LatLng(location.Latitude,location.Longitude);
            map.addMarker(new MarkerOptions().position(curPos).title("Текущая позиция").icon(BitmapDescriptorFactory.fromResource(R.drawable.arrow)));

            if(checkBoxFollow.isChecked()){  // центруем карту на мастоположение
                CameraUpdate center=CameraUpdateFactory.newLatLngZoom(curPos,16);
                map.animateCamera(center);
            }
        }
    }
}
