package com.example.BrelokAndroid;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import com.example.BrelokAndroid.BrelokConverters.BrelokDateConvertor;
import com.example.BrelokAndroid.BrelokDataStructures.BrelokListItem;

import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Admin
 * Date: 31.10.13
 * Time: 13:53
 * To change this template use File | Settings | File Templates.
 */
public class BrelokCurrentMessagesAdapter extends ArrayAdapter<BrelokListItem> {

    private  Context context;
    private final ArrayList<BrelokListItem> values;
    private static int MAX_VIEW_LENGTH = 15; // максимальная для строки для отрисовке в списке

    public BrelokCurrentMessagesAdapter(Context _context, ArrayList<BrelokListItem> objects) {
        super(_context, R.layout.listview_item, objects);
        context =_context;
        values = objects;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        // 1. Create inflater
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        // 2. Get rowView from inflater
        View rowView = inflater.inflate(R.layout.listview_item, parent, false);

        // 3. Get the two text view from the rowView
        TextView textViewMsg = (TextView) rowView.findViewById(R.id.txtMsg);
        TextView textViewDate = (TextView) rowView.findViewById(R.id.txtDate);

        // 4. Set the text for textView
        BrelokDateConvertor bdc = new BrelokDateConvertor();

        StringBuffer stringBuffer = new StringBuffer(values.get(position).getText());

        textViewMsg.setText(cutString(stringBuffer));
        textViewDate.setText(bdc.convertToShow(values.get(position).getDate()));

        if(values.get(position).getIsRead()==0){
            textViewMsg.setBackgroundColor(R.color.LightGrey);
            textViewDate.setBackgroundColor(R.color.LightGrey);
        }
        // 5. retrn rowView
        return rowView;
    }

    public BrelokListItem getClickedItem(int position){
        values.get(position).setIsRead();
        BrelokListItem blm=new BrelokListItem(values.get(position).getId(),1,values.get(position).getText(),values.get(position).getDate());
        values.remove(position);
        return blm;
    }

    public BrelokListItem getClockedItemForArchive(int position){
        BrelokListItem blm = new BrelokListItem(values.get(position).getId(),1,values.get(position).getText(),values.get(position).getDate());
        return blm;
    }

    /***
     * режет отображаемый текст, чтобы экран не мусорился
     * @param s
     * @return
     */
    private String cutString(StringBuffer s){
        if (s.length()>MAX_VIEW_LENGTH){
            s.replace(MAX_VIEW_LENGTH-3,MAX_VIEW_LENGTH,"...");
            s.delete(MAX_VIEW_LENGTH,s.length());
        }
        return s.toString();
    }
}
