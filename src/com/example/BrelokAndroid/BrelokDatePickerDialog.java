package com.example.BrelokAndroid;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;

import java.util.Calendar;

/**
 * Created with IntelliJ IDEA.
 * User: Alexey
 * Date: 27.10.13
 * Time: 15:49
 * To change this template use File | Settings | File Templates.
 */
public class BrelokDatePickerDialog extends DialogFragment {

    DatePickerDialog.OnDateSetListener ondateSet;

    public BrelokDatePickerDialog() {
    }

    public void setCallBack(DatePickerDialog.OnDateSetListener ondate) {
        ondateSet = ondate;
    }

    private int year, month, day;

    @Override
    public void setArguments(Bundle args) {
        super.setArguments(args);
        year = args.getInt("year");
        month = args.getInt("month");
        day = args.getInt("day");
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        return new DatePickerDialog(getActivity(), ondateSet, year, month, day);
    }
}
