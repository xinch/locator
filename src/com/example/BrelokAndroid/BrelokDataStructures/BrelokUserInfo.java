package com.example.BrelokAndroid.BrelokDataStructures;

/**
 * Created with IntelliJ IDEA.
 * User: Admin
 * Date: 17.10.13
 * Time: 14:31
 * To change this template use File | Settings | File Templates.
 */
public class BrelokUserInfo {

    public String login;
    public String pass;
    public String sessionId;

    public BrelokUserInfo(String _login,String _pass,String _sessionId){
        login=_login;
        pass=_pass;
        sessionId=_sessionId;
    }
}
