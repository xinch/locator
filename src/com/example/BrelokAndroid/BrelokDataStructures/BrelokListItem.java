package com.example.BrelokAndroid.BrelokDataStructures;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created with IntelliJ IDEA.
 * User: Admin
 * Date: 31.10.13
 * Time: 17:58
 * To change this template use File | Settings | File Templates.
 */
public class BrelokListItem implements Parcelable{
    private int id;
    private int isRead;
    private String text;
    private String date;
    private BrelokMsgLocation location;

    public BrelokListItem(int _id, int _isRead, String _text, String _date){
        this.id = _id;
        this.isRead = _isRead;
        this.text =_text;
        this.date = _date;
    }

    //region Getters
    public int getId(){
       return this.id;
   }

    public int getIsRead(){
        return this.isRead;
    }

    public String getText(){
       return text;
   }

    public String getDate(){
        return date;
    }

    public BrelokMsgLocation getLocation(){
        return this.location;
    }
    //endregion

    public void setIsRead(){
        this.isRead=1;
    }

    public void setLocation(BrelokMsgLocation _location){
        location=_location;
    }

    //TODO упаковка и паспакова parcel
    @Override
    public int describeContents() {
        return 0;  //To change body of implemented methods use File | Settings | File Templates.
    }

    // упаковываем объект в Parcel
    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeInt(isRead);
        dest.writeString(text);
        dest.writeString(date);
        if (location!=null){
            dest.writeDouble(location.Latitude);
            dest.writeDouble(location.Longitude);
        }

    }

    public static final Creator<BrelokListItem> CREATOR = new Parcelable.Creator<BrelokListItem>(){
        // распаковываем объект из Parcel
        public BrelokListItem createFromParcel(Parcel in){
            return new BrelokListItem(in);
        }

    public BrelokListItem[] newArray(int size){
        return new BrelokListItem[size];
    }
    };

    // конструктор, считывающий данные из Parcel
    private BrelokListItem(Parcel parcel) {
        id = parcel.readInt();
        isRead = parcel.readInt();
        text = parcel.readString();
        date = parcel.readString();
        if (location!=null){
            location.Latitude = parcel.readDouble();
            location.Longitude = parcel.readDouble();
        }
    }
}
