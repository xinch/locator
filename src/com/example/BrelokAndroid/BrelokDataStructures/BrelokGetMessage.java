package com.example.BrelokAndroid.BrelokDataStructures;

/**
 * Created with IntelliJ IDEA.
 * User: Alexey
 * Date: 29.10.13
 * Time: 23:58
 * To change this template use File | Settings | File Templates.
 */

/***
 * класс для передачи параметров задачи получения сообщений
 */
public class BrelokGetMessage {
    public String fromDate;
    public String SessionId;

    public BrelokGetMessage(String _fromdate, String _sessionId){
        fromDate=_fromdate;
        SessionId=_sessionId;
    }
}
