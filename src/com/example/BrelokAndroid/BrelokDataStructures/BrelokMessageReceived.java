package com.example.BrelokAndroid.BrelokDataStructures;

import android.location.Location;

/**
 * Created with IntelliJ IDEA.
 * User: Admin
 * Date: 17.10.13
 * Time: 14:29
 * To change this template use File | Settings | File Templates.
 */
public class BrelokMessageReceived {
    public long exID;
    public String Text;
    public long warningLevel;
    public String createTime;
    public BrelokMsgLocation Coordinates;

    public void BrelokMessagesReceived(long _exID, String _text, long _warningLevel, String _createTime, BrelokMsgLocation _coordinates){
        exID=_exID;
        Text=_text;
        warningLevel=_warningLevel;
        createTime=_createTime;
        Coordinates=_coordinates;
    }

    public void BrelokMessagesReceived(long _exID, String _text, long _warningLevel, String _createTime){
        exID=_exID;
        Text=_text;
        warningLevel=_warningLevel;
        createTime=_createTime;
        Coordinates=null;
    }
}