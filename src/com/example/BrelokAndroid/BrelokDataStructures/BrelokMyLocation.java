package com.example.BrelokAndroid.BrelokDataStructures;

import com.example.BrelokAndroid.BaseMobile.VectorGisPoint;

import java.security.PublicKey;

/**
 * Created with IntelliJ IDEA.
 * User: Alexey
 * Date: 24.10.13
 * Time: 13:59
 * To change this template use File | Settings | File Templates.
 */
public class BrelokMyLocation {

    public VectorGisPoint gisPoints;
    public String sessionId;
    public String currentTime;
    public boolean isOnShip;

   public BrelokMyLocation(VectorGisPoint _gisPoints, String _sessionId, String _currentTime, boolean _isOnShip){
       this.gisPoints = _gisPoints;
       this.sessionId = _sessionId;
       this.currentTime = _currentTime;
       this.isOnShip = _isOnShip;
    }
}
