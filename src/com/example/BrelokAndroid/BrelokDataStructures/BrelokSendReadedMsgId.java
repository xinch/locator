package com.example.BrelokAndroid.BrelokDataStructures;

import com.example.BrelokAndroid.BaseMobile.VectorInt64;

/**
 * Created by Admin on 12.12.13.
 */
public class BrelokSendReadedMsgId {

    public String SessionId;
    public VectorInt64 ids;

    public BrelokSendReadedMsgId (VectorInt64 _ids,String _sessionId){
        ids=_ids;
        SessionId=_sessionId;
    }
}
