package com.example.BrelokAndroid.BrelokDataStructures;

import com.example.BrelokAndroid.BaseMobile.VectorInt64;

/**
 * Created with IntelliJ IDEA.
 * User: Alexey
 * Date: 29.10.13
 * Time: 23:55
 * To change this template use File | Settings | File Templates.
 */
/***
 * класс для передачи параметров задачи отправки потверждения прихода сообщений от сервиса
 */
public class BrelokGetMessagesByID {
    public VectorInt64 Ids;
    public String SessionId;

    public BrelokGetMessagesByID(VectorInt64 ids, String sessionId){
        Ids=ids;
        SessionId=sessionId;
    }
}
