package com.example.BrelokAndroid.Tests;

import com.example.BrelokAndroid.BaseMobile.GetMessageAnswer;
import com.example.BrelokAndroid.BaseMobile.LoginAnswer;
import com.example.BrelokAndroid.BaseMobile.Message;
import com.example.BrelokAndroid.BaseMobile.VectorMessage;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

/**
 * Created with IntelliJ IDEA.
 * User: Admin
 * Date: 31.10.13
 * Time: 10:43
 * To change this template use File | Settings | File Templates.
 */
public class MessageGenerator {

    public static GetMessageAnswer generatMsg(int count){
        GetMessageAnswer msg = new GetMessageAnswer();
        msg.messages = new VectorMessage();
        for (int i=0;i<=count;i++){
            // add loginansewer data
            LoginAnswer loginAnswer = new LoginAnswer();
            loginAnswer.isLoggedIn=true;
            loginAnswer.sessionId="iiii";

            msg.lA=loginAnswer;
            //new message
            Message message = new Message();
            message.id=i;
            //current time
            Date currentTime = new Date();
            SimpleDateFormat sdf = new SimpleDateFormat("EEE, MMM d, yyyy HH:mm:ss a z");
            sdf.setTimeZone(TimeZone.getTimeZone("GMT"));
            message.dateCreate=sdf.format(currentTime);
            message.isReaded=false;
            message.point=null;
            message.msg="lala "+i;
            message.priority=i;
            //adding message
            msg.messages.add(message);
        }
        return msg;
    }
}
