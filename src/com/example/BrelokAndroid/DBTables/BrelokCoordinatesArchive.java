package com.example.BrelokAndroid.DBTables;

/**
 * Created with IntelliJ IDEA.
 * User: Admin
 * Date: 14.10.13
 * Time: 8:54
 * To change this template use File | Settings | File Templates.
 */
public interface BrelokCoordinatesArchive {
    /**
     * таблица хранит архив координат устройства
     */
    public static final String coordinatesArchive ="BrelokCoordinatesArchive";

    // поля BrelokCoordinatesArchive
    public static final String colLocationID="_id";
    public static final String colLongitude="Longitude"; // долгота
    public static final String colLatitude="Latitude";  // широта
    public static final String colAltitude="Altitude";
    public static final String colAccurancy="Accurancy";
    public static final String colBearing="Bearing";
    public static final String colSpeed="Speed";
    public static final String colDateTtime="DateTime";
    // поля BrelokCoordinatesArchive
}
