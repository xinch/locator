package com.example.BrelokAndroid.DBTables;

/**
 * Created with IntelliJ IDEA.
 * User: Admin
 * Date: 14.10.13
 * Time: 8:55
 * To change this template use File | Settings | File Templates.
 */
public interface BrelokCoordinatesReceived {
    /**
     * таблица хранит координаты, принятные от сервиса. Связана с таблицей BrelokMessageArchive
     */
    public static final String coordinatesReceived="BrelokCoordinatesReceived";

    //поля BrelokCoordinatesReceived
    public static final String colLocationID="id";
    public static final String colLongitude="Longitude"; // долгота
    public static final String colLatitude="Latitude";  // широта
    public static final String colAltitude="Altitude";
    //поля BrelokCoordinatesReceived
}
