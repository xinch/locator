package com.example.BrelokAndroid.DBTables;

/**
 * Created with IntelliJ IDEA.
 * User: Alexey
 * Date: 24.10.13
 * Time: 0:47
 * To change this template use File | Settings | File Templates.
 */
public interface BrelokSettings {
    /**
     * таблица хранит настройки системы .
     */

    public static final String settings ="BrelokSettings";

    // поля BrelokSettings
    public static final String colID="_id";
    public static final String colServiceUrl="ServiceUrl";
    public static final String colServiceTimeout="ServiceTimeout";
    public static final String colGpsAccuracy="GPSAccuracy";
    public static final String colNETWORKAccuracy="NETWORKAccuracy";
    public static final String colGetCoordinatesTimeout="GetCoordinatesTimeout";
    public static final String colServiceExchangeTimeout="ServiceExchangeTimeout";
}
