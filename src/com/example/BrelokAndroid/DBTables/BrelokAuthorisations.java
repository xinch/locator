package com.example.BrelokAndroid.DBTables;

/**
 * Created with IntelliJ IDEA.
 * User: Admin
 * Date: 14.10.13
 * Time: 8:55
 * To change this template use File | Settings | File Templates.
 */
public interface BrelokAuthorisations {
    /**
     * таблица хранит текущую авторизацию пользоваттеля.
     */
    public static final String authorisations ="BrelokAuthorisations";

    //поля BrelokAuthorisations
    public static final String colID="_id";
    public static final String colLogin="Login"; //
    public static final String colPassword="Password";  //
    public static final String colSessionId="SessionId";
    public static final String colIsLogined="IsLogined";
    public static final String colIsOnAShip="isOnAShip";
    //поля BrelokAuthorisations
}
