package com.example.BrelokAndroid.DBTables;

/**
 * Created with IntelliJ IDEA.
 * User: Admin
 * Date: 14.10.13
 * Time: 8:54
 * To change this template use File | Settings | File Templates.
 */
public interface BrelokMessagesArchive {
    /**
     * хранит архив принятых с сервера сообщений. Связана с таблицей BrelokCoordinatesReceived через поле Coordinates
     */
    public static final String messagesArchive ="BrelokMessagesArchive";

    // поля BrelokMessagesArchive
    public static final String colMessageID="_id";
    public static final String colExternalID="ExternalID";
    public static final String colText="Text";
    public static final String colReaded="IsRead";
    public static final String colWarningLevel="WarningLevel";
    public static final String colCreateTime ="CreateTime";
    public static final String colCoordinates="Coordinates";
    // поля BrelokMessagesArchive
}
