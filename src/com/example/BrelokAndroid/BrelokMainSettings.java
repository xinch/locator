package com.example.BrelokAndroid;

/**
 * Created with IntelliJ IDEA.
 * User: Alexey
 * Date: 30.10.13
 * Time: 21:09
 * To change this template use File | Settings | File Templates.
 */
public interface BrelokMainSettings {
    //TODO Все нижеперечисленные параметры брать из базы
    public static final int RESUME_LOCATION = 1;
    public static final int PAUSE_LOCATION = 2;
    public static final int UPDATE_PROVIDER = 3;
    public static final int GPS_ACCURACY = 20;
    public static final int NETWORK_ACCURACY = 150;
    public static final int SLEEP_TIME_GPS = 220; // время между запусками gps, в с
    public static final int SLEEP_TIME_SERVICE= 180; //время между запусками обмена, в с
    // параметры для broadcast
    public static final int MAP_UPDATE_TASK=1;   // апдейт карты
    public static final int LIST_UPDATE_TASK=2;  // апдейт листа
}
