package com.example.BrelokAndroid.BaseMobile;

import org.ksoap2.serialization.Marshal;
import org.ksoap2.serialization.PropertyInfo;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlSerializer;

import java.io.IOException;

/**
 * Created with IntelliJ IDEA.
 * User: Admin
 * Date: 29.11.13
 * Time: 13:41
 * To change this template use File | Settings | File Templates.
 */
public class MarshaGisPoints implements Marshal {

    public MarshaGisPoints(){

    }

    private static class MarshaGisPointsHolder {

        public static MarshaGisPoints instance = new MarshaGisPoints();

    }

    public static MarshaGisPoints getInstance(){
        return MarshaGisPointsHolder.instance;
    }

    @Override
    public Object readInstance(XmlPullParser xmlPullParser, String s, String s2, PropertyInfo propertyInfo) throws IOException, XmlPullParserException {
        return xmlPullParser.next();  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public void writeInstance(XmlSerializer xmlSerializer, Object o) throws IOException {
        final String tem = "http://tempuri.org/";
        final String ser = "http://schemas.datacontract.org/2004/07/ServiceForMobile" ;

        VectorGisPoint gisPoints=(VectorGisPoint) o;
        for (int i=0;i<gisPoints.size();i++){
            xmlSerializer.startTag(ser,"GisPoint");

            xmlSerializer.startTag(ser,"Accuracy");
            xmlSerializer.text(Double.toString(gisPoints.elementAt(i).accuracy));
            xmlSerializer.endTag(ser,"Accuracy");

            xmlSerializer.startTag(ser,"Altitude");
            xmlSerializer.text(Double.toString(gisPoints.elementAt(i).altitude));
            xmlSerializer.endTag(ser,"Altitude");

            xmlSerializer.startTag(ser,"Bearing");
            xmlSerializer.text(Double.toString(gisPoints.elementAt(i).bearing));
            xmlSerializer.endTag(ser,"Bearing");

            xmlSerializer.startTag(ser,"Latitude");
            xmlSerializer.text(Double.toString(gisPoints.elementAt(i).latitude));
            xmlSerializer.endTag(ser,"Latitude");

            xmlSerializer.startTag(ser,"Longitude");
            xmlSerializer.text(Double.toString(gisPoints.elementAt(i).longitude));
            xmlSerializer.endTag(ser,"Longitude");

            xmlSerializer.startTag(ser,"Speed");
            xmlSerializer.text(Double.toString(gisPoints.elementAt(i).speed));
            xmlSerializer.endTag(ser,"Speed");

            xmlSerializer.startTag(ser,"TimeStamp");
            xmlSerializer.text(gisPoints.elementAt(i).timeStamp);
            xmlSerializer.endTag(ser,"TimeStamp");

            xmlSerializer.endTag(ser,"GisPoint");
        }
        //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public void register(SoapSerializationEnvelope soapSerializationEnvelope) {
        soapSerializationEnvelope.addMapping("http://tempuri.org/","GisPoint",VectorGisPoint.class,MarshaGisPoints.getInstance());
        //To change body of implemented methods use File | Settings | File Templates.
    }
}
