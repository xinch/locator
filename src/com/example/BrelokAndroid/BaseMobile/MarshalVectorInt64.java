package com.example.BrelokAndroid.BaseMobile;

import android.view.ViewDebug;
import org.ksoap2.serialization.Marshal;
import org.ksoap2.serialization.PropertyInfo;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlSerializer;

import java.io.IOException;

/**
 * Created by Admin on 12.12.13.
 */
public class MarshalVectorInt64 implements Marshal {

    public MarshalVectorInt64(){

    }

    private static class MarshalVectorInt64Holder {

        public static MarshalVectorInt64 instance = new MarshalVectorInt64();

    }

    public static MarshalVectorInt64 getInstance(){
        return MarshalVectorInt64Holder.instance;
    }

    @Override
    public Object readInstance(XmlPullParser xmlPullParser, String s, String s2, PropertyInfo propertyInfo) throws IOException, XmlPullParserException {
        return xmlPullParser.next();
    }

    @Override
    public void writeInstance(XmlSerializer xmlSerializer, Object o) throws IOException {
        final String tem = "http://tempuri.org/";
        final String ser = "http://schemas.datacontract.org/2004/07/ServiceForMobile" ;
        final String arr = "http://schemas.microsoft.com/2003/10/Serialization/Arrays";

        VectorInt64 ids = (VectorInt64) o;
        for(int i=0; i<ids.size();i++){

            xmlSerializer.startTag(arr,"long");
            xmlSerializer.text(String.valueOf(ids.elementAt(i).intValue()));
            xmlSerializer.endTag(arr,"long");

        }
    }

    @Override
    public void register(SoapSerializationEnvelope soapSerializationEnvelope) {
        soapSerializationEnvelope.addMapping("http://tempuri.org/","long",VectorInt64.class,MarshalVectorInt64.getInstance());
    }
}
