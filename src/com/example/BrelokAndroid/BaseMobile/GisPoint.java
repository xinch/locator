package com.example.BrelokAndroid.BaseMobile;

//------------------------------------------------------------------------------
// <wsdl2code-generated>
//    This code was generated by http://www.wsdl2code.com version  2.5
//
// Date Of Creation: 12/6/2013 9:18:51 AM
//    Please dont change this code, regeneration will override your changes
//</wsdl2code-generated>
//
//------------------------------------------------------------------------------
//
//This source code was auto-generated by Wsdl2Code  Version
//
import org.ksoap2.serialization.KvmSerializable;
import org.ksoap2.serialization.PropertyInfo;
import java.util.Hashtable;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapPrimitive;

public class GisPoint implements KvmSerializable {
    
    public double accuracy;
    public boolean accuracySpecified;
    public double altitude;
    public boolean altitudeSpecified;
    public double bearing;
    public boolean bearingSpecified;
    public double latitude;
    public boolean latitudeSpecified;
    public double longitude;
    public boolean longitudeSpecified;
    public double speed;
    public boolean speedSpecified;
    public String timeStamp;
    public boolean timeStampSpecified;
    
    public GisPoint(){}
    
    public GisPoint(SoapObject soapObject)
    {
        if (soapObject == null)
            return;
        if (soapObject.hasProperty("Accuracy"))
        {
            Object obj = soapObject.getProperty("Accuracy");
            if (obj != null && obj.getClass().equals(SoapPrimitive.class)){
                SoapPrimitive j =(SoapPrimitive) obj;
                accuracy = Double.parseDouble(j.toString());
            }else if (obj!= null && obj instanceof Number){
                accuracy = (Double) obj;
            }
        }
        if (soapObject.hasProperty("AccuracySpecified"))
        {
            Object obj = soapObject.getProperty("AccuracySpecified");
            if (obj != null && obj.getClass().equals(SoapPrimitive.class)){
                SoapPrimitive j =(SoapPrimitive) obj;
                accuracySpecified = Boolean.parseBoolean(j.toString());
            }else if (obj!= null && obj instanceof Boolean){
                accuracySpecified = (Boolean) obj;
            }
        }
        if (soapObject.hasProperty("Altitude"))
        {
            Object obj = soapObject.getProperty("Altitude");
            if (obj != null && obj.getClass().equals(SoapPrimitive.class)){
                SoapPrimitive j =(SoapPrimitive) obj;
                altitude = Double.parseDouble(j.toString());
            }else if (obj!= null && obj instanceof Number){
                altitude = (Double) obj;
            }
        }
        if (soapObject.hasProperty("AltitudeSpecified"))
        {
            Object obj = soapObject.getProperty("AltitudeSpecified");
            if (obj != null && obj.getClass().equals(SoapPrimitive.class)){
                SoapPrimitive j =(SoapPrimitive) obj;
                altitudeSpecified = Boolean.parseBoolean(j.toString());
            }else if (obj!= null && obj instanceof Boolean){
                altitudeSpecified = (Boolean) obj;
            }
        }
        if (soapObject.hasProperty("Bearing"))
        {
            Object obj = soapObject.getProperty("Bearing");
            if (obj != null && obj.getClass().equals(SoapPrimitive.class)){
                SoapPrimitive j =(SoapPrimitive) obj;
                bearing = Double.parseDouble(j.toString());
            }else if (obj!= null && obj instanceof Number){
                bearing = (Double) obj;
            }
        }
        if (soapObject.hasProperty("BearingSpecified"))
        {
            Object obj = soapObject.getProperty("BearingSpecified");
            if (obj != null && obj.getClass().equals(SoapPrimitive.class)){
                SoapPrimitive j =(SoapPrimitive) obj;
                bearingSpecified = Boolean.parseBoolean(j.toString());
            }else if (obj!= null && obj instanceof Boolean){
                bearingSpecified = (Boolean) obj;
            }
        }
        if (soapObject.hasProperty("Latitude"))
        {
            Object obj = soapObject.getProperty("Latitude");
            if (obj != null && obj.getClass().equals(SoapPrimitive.class)){
                SoapPrimitive j =(SoapPrimitive) obj;
                latitude = Double.parseDouble(j.toString());
            }else if (obj!= null && obj instanceof Number){
                latitude = (Double) obj;
            }
        }
        if (soapObject.hasProperty("LatitudeSpecified"))
        {
            Object obj = soapObject.getProperty("LatitudeSpecified");
            if (obj != null && obj.getClass().equals(SoapPrimitive.class)){
                SoapPrimitive j =(SoapPrimitive) obj;
                latitudeSpecified = Boolean.parseBoolean(j.toString());
            }else if (obj!= null && obj instanceof Boolean){
                latitudeSpecified = (Boolean) obj;
            }
        }
        if (soapObject.hasProperty("Longitude"))
        {
            Object obj = soapObject.getProperty("Longitude");
            if (obj != null && obj.getClass().equals(SoapPrimitive.class)){
                SoapPrimitive j =(SoapPrimitive) obj;
                longitude = Double.parseDouble(j.toString());
            }else if (obj!= null && obj instanceof Number){
                longitude = (Double) obj;
            }
        }
        if (soapObject.hasProperty("LongitudeSpecified"))
        {
            Object obj = soapObject.getProperty("LongitudeSpecified");
            if (obj != null && obj.getClass().equals(SoapPrimitive.class)){
                SoapPrimitive j =(SoapPrimitive) obj;
                longitudeSpecified = Boolean.parseBoolean(j.toString());
            }else if (obj!= null && obj instanceof Boolean){
                longitudeSpecified = (Boolean) obj;
            }
        }
        if (soapObject.hasProperty("Speed"))
        {
            Object obj = soapObject.getProperty("Speed");
            if (obj != null && obj.getClass().equals(SoapPrimitive.class)){
                SoapPrimitive j =(SoapPrimitive) obj;
                speed = Double.parseDouble(j.toString());
            }else if (obj!= null && obj instanceof Number){
                speed = (Double) obj;
            }
        }
        if (soapObject.hasProperty("SpeedSpecified"))
        {
            Object obj = soapObject.getProperty("SpeedSpecified");
            if (obj != null && obj.getClass().equals(SoapPrimitive.class)){
                SoapPrimitive j =(SoapPrimitive) obj;
                speedSpecified = Boolean.parseBoolean(j.toString());
            }else if (obj!= null && obj instanceof Boolean){
                speedSpecified = (Boolean) obj;
            }
        }
        if (soapObject.hasProperty("TimeStamp"))
        {
            Object obj = soapObject.getProperty("TimeStamp");
            if (obj != null && obj.getClass().equals(SoapPrimitive.class)){
                SoapPrimitive j =(SoapPrimitive) obj;
                timeStamp = j.toString();
            }else if (obj!= null && obj instanceof String){
                timeStamp = (String) obj;
            }
        }
        if (soapObject.hasProperty("TimeStampSpecified"))
        {
            Object obj = soapObject.getProperty("TimeStampSpecified");
            if (obj != null && obj.getClass().equals(SoapPrimitive.class)){
                SoapPrimitive j =(SoapPrimitive) obj;
                timeStampSpecified = Boolean.parseBoolean(j.toString());
            }else if (obj!= null && obj instanceof Boolean){
                timeStampSpecified = (Boolean) obj;
            }
        }
    }
    @Override
    public Object getProperty(int arg0) {
        switch(arg0){
            case 0:
                return accuracy;
            case 1:
                return accuracySpecified;
            case 2:
                return altitude;
            case 3:
                return altitudeSpecified;
            case 4:
                return bearing;
            case 5:
                return bearingSpecified;
            case 6:
                return latitude;
            case 7:
                return latitudeSpecified;
            case 8:
                return longitude;
            case 9:
                return longitudeSpecified;
            case 10:
                return speed;
            case 11:
                return speedSpecified;
            case 12:
                return timeStamp;
            case 13:
                return timeStampSpecified;
        }
        return null;
    }
    
    @Override
    public int getPropertyCount() {
        return 14;
    }
    
    @Override
    public void getPropertyInfo(int index, @SuppressWarnings("rawtypes") Hashtable arg1, PropertyInfo info) {
        switch(index){
            case 0:
                info.type = Double.class;
                info.name = "Accuracy";
                break;
            case 1:
                info.type = PropertyInfo.BOOLEAN_CLASS;
                info.name = "AccuracySpecified";
                break;
            case 2:
                info.type = Double.class;
                info.name = "Altitude";
                break;
            case 3:
                info.type = PropertyInfo.BOOLEAN_CLASS;
                info.name = "AltitudeSpecified";
                break;
            case 4:
                info.type = Double.class;
                info.name = "Bearing";
                break;
            case 5:
                info.type = PropertyInfo.BOOLEAN_CLASS;
                info.name = "BearingSpecified";
                break;
            case 6:
                info.type = Double.class;
                info.name = "Latitude";
                break;
            case 7:
                info.type = PropertyInfo.BOOLEAN_CLASS;
                info.name = "LatitudeSpecified";
                break;
            case 8:
                info.type = Double.class;
                info.name = "Longitude";
                break;
            case 9:
                info.type = PropertyInfo.BOOLEAN_CLASS;
                info.name = "LongitudeSpecified";
                break;
            case 10:
                info.type = Double.class;
                info.name = "Speed";
                break;
            case 11:
                info.type = PropertyInfo.BOOLEAN_CLASS;
                info.name = "SpeedSpecified";
                break;
            case 12:
                info.type = PropertyInfo.STRING_CLASS;
                info.name = "TimeStamp";
                break;
            case 13:
                info.type = PropertyInfo.BOOLEAN_CLASS;
                info.name = "TimeStampSpecified";
                break;
        }
    }
    
    @Override
    public void setProperty(int arg0, Object arg1) {
    }
    
}
