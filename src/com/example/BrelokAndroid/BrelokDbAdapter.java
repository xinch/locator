package com.example.BrelokAndroid;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.location.Location;
import com.example.BrelokAndroid.BrelokDataStructures.BrelokMsgLocation;
import com.example.BrelokAndroid.BrelokDataStructures.BrelokUserInfo;
import com.example.BrelokAndroid.DBTables.*;
import com.example.BrelokAndroid.BrelokDataStructures.BrelokMessageReceived;

import java.util.Date;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Created with IntelliJ IDEA.
 * User: Admin
 * Date: 30.09.13
 * Time: 11:45
 * To change this template use File | Settings | File Templates.
 */
public class BrelokDbAdapter implements BrelokCoordinatesArchive, BrelokAuthorisations,BrelokCoordinatesReceived,BrelokMessagesArchive {

    private Context context;
    private SQLiteDatabase database;
    private BrelokSqlDBHelper dbHelper;

    private BrelokDatabaseManager databaseManager;

    public BrelokDbAdapter(Context context){
        this.context=context;
    }

    //region Helpers

    public void openHelper(){
        dbHelper=new BrelokSqlDBHelper(context);
        databaseManager.initializeInstance(dbHelper);
        //database=dbHelper.getWritableDatabase();
        database = databaseManager.getInstance().openDatabase();
    }

    public void closeHelper(){
       // dbHelper.close();

        databaseManager.getInstance().closeDatabase();
        //database.close();
    }

    //endregion

    //region Work with BrelokCoordinatesArchive table

    public void insertCoordinates(Location location, String date){
        try {
            database.beginTransaction();

            ContentValues cv = new ContentValues();
            cv.put(BrelokCoordinatesArchive.colLongitude,location.getLongitude());
            cv.put(BrelokCoordinatesArchive.colLatitude,location.getLatitude());
            cv.put(BrelokCoordinatesArchive.colAccurancy,location.getAccuracy());
            cv.put(BrelokCoordinatesArchive.colAltitude,location.getAltitude());
            cv.put(BrelokCoordinatesArchive.colSpeed,location.getSpeed());
            cv.put(BrelokCoordinatesArchive.colBearing,location.getBearing());
            cv.put(BrelokCoordinatesArchive.colDateTtime,date.toString());
            database.insert(BrelokCoordinatesArchive.coordinatesArchive,BrelokCoordinatesArchive.colLocationID,cv);

            database.setTransactionSuccessful();
        }finally {
            database.endTransaction();
        }
    }

    /**
     * чистит таблицу своего местоположения, оставляя одну последнюю запись
     */
    public void clearMyCoordinates(){
        // DELETE FROM BrelokCoordinatesArchive WHERE _id not in (SELECT MAX(_id) FROM BrelokCoordinatesArchive)
        try{
            database.beginTransaction();
            database.execSQL("DELETE FROM "+BrelokCoordinatesArchive.coordinatesArchive+" WHERE "+BrelokCoordinatesArchive.colLocationID+" NOT IN ("
                    +getLastID(BrelokCoordinatesArchive.coordinatesArchive,BrelokCoordinatesArchive.colLocationID)+")");
            database.setTransactionSuccessful();
        } finally {
            database.endTransaction();
        }
    }

    //region Cursor returns methods
    public Cursor getAllCoordinatesArchive(){
        // SELECT * FROM BrelokCoordinatesArchive
        Cursor cursor;
        try{
            database.beginTransaction();
            cursor = database.rawQuery("SELECT * FROM " + BrelokCoordinatesArchive.coordinatesArchive, null);
            database.setTransactionSuccessful();
        }finally {
            database.endTransaction();
        }
        return cursor;
    }

    public Cursor getLastDeviceCoordinates(){
        // SELECT * FROM BrelokCoordinatesArchive  WHERE _id =(SELECT MAX(_id) FROM BrelokCoordinatesArchive)
        int id = getLastID(BrelokCoordinatesArchive.coordinatesArchive,"_id");
        if (id>0){
            Cursor cursor;
            try{
                database.beginTransaction();
                cursor=database.rawQuery("SELECT * FROM "+BrelokCoordinatesArchive.coordinatesArchive+" WHERE _id="+id,null);
                database.setTransactionSuccessful();
            }finally {
                database.endTransaction();
            }
            return cursor;
        }
        return null;
    }
    //endregion
    //endregion

    //region Work with BrelokCoordinatesReceived table

    public void coordinatesReceivedInsert(BrelokMsgLocation location){
        try {
            database.beginTransaction();
            ContentValues cv = new ContentValues();
            cv.put(BrelokCoordinatesReceived.colLongitude,location.Longitude);
            cv.put(BrelokCoordinatesReceived.colLatitude,location.Latitude);
            //cv.put(BrelokCoordinatesReceived.colAltitude,location.getAltitude());
            database.insert(BrelokCoordinatesReceived.coordinatesReceived,colID,cv);
            database.setTransactionSuccessful();
        }finally {
            database.endTransaction();
        }
    }

    public Cursor getCoordinatesById(int id){
        //SELECT * FROM BrelokCoordinatesReceived WHERE BrelokCoordinatesReceived._id=(SELECT  BrelokMessagesArchive.Coordinates FROM BrelokMessagesArchive WHERE BrelokMessagesArchive._id=2)
        Cursor cursor;
        try{
            database.beginTransaction();
            cursor = database.rawQuery("SELECT * FROM "+BrelokCoordinatesReceived.coordinatesReceived
                    + " WHERE "+BrelokCoordinatesReceived.coordinatesReceived+"."+BrelokCoordinatesReceived.colLocationID
                    +"=(SELECT "+BrelokMessagesArchive.messagesArchive+"."+BrelokMessagesArchive.colCoordinates
                    +" FROM "+ BrelokMessagesArchive.messagesArchive+" WHERE "
                    +BrelokMessagesArchive.messagesArchive+"."+BrelokMessagesArchive.colMessageID+"="+id+")",null);
            database.setTransactionSuccessful();
        }finally {
            database.endTransaction();
        }
        return cursor;
    }
    //endregion

    //region Work with BrelokMessagesArchive table

    /***
     * вставка пришедшего сообщения в базу. Идет определения, пришли ли координаты. Если пришли, то сначала пишутся они, потом пишется сообщение и ссылка на запись  с координатами
     * @param msg объект сообщения
     */
    public void insertMessage(BrelokMessageReceived msg){
            try{
                //начинаем писать сообщение
                database.beginTransaction();
                ContentValues cv = new ContentValues();
                cv.put(BrelokMessagesArchive.colExternalID,msg.exID);
                cv.put(BrelokMessagesArchive.colText,msg.Text);
                cv.put(BrelokMessagesArchive.colReaded,0);
                cv.put(BrelokMessagesArchive.colWarningLevel,msg.warningLevel);
                cv.put(BrelokMessagesArchive.colCreateTime,msg.createTime);
                if(msg.Coordinates!=null){  // значит есть координаты
                    coordinatesReceivedInsert(msg.Coordinates);// вставляем координаты
                    int lastID = getLastID(BrelokCoordinatesReceived.coordinatesReceived,BrelokCoordinatesReceived.colLocationID); // получаем id только что записанных координат
                    cv.put(BrelokMessagesArchive.colCoordinates,lastID); // вставляем id пришедший координат
                }
                database.insert(BrelokMessagesArchive.messagesArchive,BrelokMessagesArchive.colMessageID,cv);
                database.setTransactionSuccessful();
            }   finally {
                database.endTransaction();
            }
    }

    public void setMessageRead(int id){
        // UPDATE BrelokMessagesArchive SET IsRead=1 WHERE BrelokMessagesArchive._id=16
        try{
            database.beginTransaction();
            database.execSQL("UPDATE "+BrelokMessagesArchive.messagesArchive+" SET "+BrelokMessagesArchive.colReaded+"=1 WHERE "+BrelokMessagesArchive.messagesArchive
                   +"."+BrelokMessagesArchive.colMessageID+"="+id,new Object[]{} );

            database.setTransactionSuccessful();
        } finally {
            database.endTransaction();
        }
    }

    //region Cursor returns methods

    /***
     * ищет все сообщения, которые были приняты с заданной даты. Нужно для отправки id на сервис для потверждния
     * @param date дата, начиная с которой выбираются сообщения
     * @return курсор id'шников
     */
   /* public Cursor getMessagesFromDateToDate(String dateStart,String dateEnd){
        //SELECT ExternalId FROM BrelokMessagesArchive WHERE CreateTime>= '2013-10-04'
        Cursor cursor;
        try{
            database.beginTransaction();
            cursor=database.rawQuery("SELECT "+BrelokMessagesArchive.colExternalID+" FROM "+ BrelokMessagesArchive.messagesArchive
                    +" WHERE "+BrelokMessagesArchive.colCreateTime+">='"+date+"'",null);
            database.setTransactionSuccessful();
        }finally {
            database.endTransaction();
        }
        return cursor;
    } */

    public Cursor getMessageById(int id){
        //SELECT * FROM BrelokMessagesArchive LEFT JOIN BrelokCoordinatesReceived ON BrelokMessagesArchive.Coordinates=BrelokCoordinatesReceived._id WHERE BrelokMessagesArchive._id=3
        Cursor cursor;
        try{
            database.beginTransaction();
            cursor = database.rawQuery("SELECT * FROM "+BrelokMessagesArchive.messagesArchive+" LEFT JOIN "+BrelokCoordinatesReceived.coordinatesReceived
                    +" ON "+BrelokMessagesArchive.messagesArchive+"."+BrelokMessagesArchive.colCoordinates+"="
                    +BrelokCoordinatesReceived.coordinatesReceived+"."+BrelokCoordinatesReceived.colLocationID
                    +" WHERE "+BrelokMessagesArchive.messagesArchive+"."+BrelokMessagesArchive.colMessageID+"="+id,null);
            database.setTransactionSuccessful();
        }finally {
            database.endTransaction();
        }
        return cursor;
    }

    /***
     * выдает все непрочитанные сообщения (Readed!=1)
     * @return
     */
    public Cursor getAllUnreadedMessages(){
        // SELECT * FROM BrelokMessagesArchive LEFT JOIN BrelokCoordinatesReceived ON BrelokMessagesArchive.Coordinates=BrelokCoordinatesReceived._id WHERE IsRead!=1
        Cursor cursor;
        try {
            database.beginTransaction();
            cursor=database.rawQuery("SELECT * FROM "+BrelokMessagesArchive.messagesArchive+" LEFT JOIN "+BrelokCoordinatesReceived.coordinatesReceived
                    +" ON "+BrelokMessagesArchive.messagesArchive+"."+BrelokMessagesArchive.colCoordinates+"="
                    +BrelokCoordinatesReceived.coordinatesReceived+"."+BrelokCoordinatesReceived.colLocationID
                    +" WHERE "+BrelokMessagesArchive.colReaded+" != 1",null);
            database.setTransactionSuccessful();
        }finally {
            database.endTransaction();
        }
        return cursor;
    }

    /***
     * выдает все прочитанные сообщейния
     * @return
     */
    public Cursor getAllReadedMessages(){
        //SELECT * FROM BrelokMessagesArchive LEFT JOIN BrelokCoordinatesReceived ON BrelokMessagesArchive.Coordinates=BrelokCoordinatesReceived._id WHERE IsRead!=0
        Cursor cursor;
        try{
            database.beginTransaction();
            cursor = database.rawQuery("SELECT * FROM "+BrelokMessagesArchive.messagesArchive+" LEFT JOIN "+BrelokCoordinatesReceived.coordinatesReceived
                    +" ON "+BrelokMessagesArchive.messagesArchive+"."+BrelokMessagesArchive.colCoordinates+"="
                    +BrelokCoordinatesReceived.coordinatesReceived+"."+BrelokCoordinatesReceived.colLocationID
                    +" WHERE "+BrelokMessagesArchive.colReaded+" != 0",null);
            database.setTransactionSuccessful();
        }finally {
            database.endTransaction();
        }
        return cursor;
    }

    /***
     * выдает вообще все непрочитанные сообщения без координат. Понятию не имею, зачем написал этот метод
     * @return
     */
    //TODO delete this
    public Cursor getAllCurrentMessages(){
        // SELECT * FROM BrelokMessagesArchive, BrelokCoordinatesReceived WHERE BrelokMessagesArchive.Coordinates=BrelokCoordinatesReceived._id
        Cursor cursor;
        try{
            database.beginTransaction();
            cursor=database.rawQuery("SELECT * FROM "+ BrelokMessagesArchive.messagesArchive +" WHERE "+BrelokMessagesArchive.colReaded+" != 1 ",null);
            database.setTransactionSuccessful();
        } finally {
            database.endTransaction();
        }
        return cursor;
    }
    //endregion

    public String getLastDateMessage(){
        // SELECT BrelokMessagesArchive.CreateTime FROM  BrelokMessagesArchive WHERE _id = (SELECT MAX(_id) FROM BrelokMessagesArchive)
        Cursor cursor;
        try{
            database.beginTransaction();
            cursor=database.rawQuery("SELECT "+BrelokMessagesArchive.colCreateTime+" FROM "+BrelokMessagesArchive.messagesArchive+" WHERE _id = (SELECT MAX(_id) FROM "+BrelokMessagesArchive.messagesArchive+" )" ,null);
            database.setTransactionSuccessful();
        }finally {
            database.endTransaction();
        }
        if(cursor.getCount()>0){
            cursor.moveToFirst();
            return  cursor.getString(cursor.getColumnIndex(BrelokMessagesArchive.colCreateTime));
        } else return null;
    }
    //endregion

    //region Work with BrelokQuerry

    /***
     * вставка в очередь очередной записи к отправке
     * @param tableName название натблицы
     * @param id id записи
     */
    public void insertQuerry(String tableName, int id){
        try{
        database.beginTransaction();
        ContentValues cv = new ContentValues();
        cv.put(BrelokQuerry.colTableName,tableName);
        cv.put(BrelokQuerry.colIdToSend,id);
        database.insert(BrelokQuerry.querry,BrelokQuerry.colID,cv);
        database.setTransactionSuccessful();
        }finally {
            database.endTransaction();
        }
    }

    /**
     * чистит из очереди записи для определенной таблицы
     * @param tableName название таблицы, для которой пора чистить очередь
     */
    public void clearQuerry(String tableName){
        //TODO написать запрос
        try{
            database.beginTransaction();
            database.execSQL("DELETE FROM "+BrelokQuerry.querry+" WHERE "+BrelokQuerry.colTableName+" = '"+tableName+"'");
            database.setTransactionSuccessful();
        }finally {
            database.endTransaction();
        }
    }

    /***
     * метод ищет в очереди все координаты к отправке
     * @return курсор содержит координаты к отправке
     */
    public Cursor getCoordinatesToSend(){
        //SELECT * FROM BrelokCoordinatesArchive WHERE _id IN (SELECT IdToSend FROM BrelokQuerry WHERE TableName='BrelokCoordinatesArchive')
        Cursor cursor;
       try{
           database.beginTransaction();
           cursor=database.rawQuery("SELECT * FROM "+BrelokCoordinatesArchive.coordinatesArchive
                   +" WHERE _id IN (SELECT IdToSend FROM "+BrelokQuerry.querry+" WHERE TableName ='"+BrelokCoordinatesArchive.coordinatesArchive+"')",null);
           database.setTransactionSuccessful();
       }finally {
           database.endTransaction();
       }
        return cursor;
    }

    /***
     * ищет все прочитанные сообщениея, которые нужно отправить
     * @return
     */
    public Cursor getReadedMessagesId(){
        // написать запрос текстом
        Cursor cursor;
        try{
            database.beginTransaction();
            cursor=database.rawQuery("SELECT " + BrelokMessagesArchive.colExternalID+" FROM "+BrelokMessagesArchive.messagesArchive
                    +" WHERE "+BrelokMessagesArchive.colMessageID+"  IN (SELECT IdToSend FROM "+BrelokQuerry.querry+" WHERE TableName ='"+BrelokMessagesArchive.messagesArchive+"')",null);
            database.setTransactionSuccessful();
        }finally {
            database.endTransaction();
        }

        return cursor;
    }
    //endregion

    //region Work with BrelokAuthorisations

    //region Void methods
    /**
     * разлогин
     * @param user
     */
    public void logout(BrelokUserInfo user){
        //UPDATE BrelokAuthorisations SET IsLogined=0,SessionId='' WHERE Login='admin'
        try{
            database.beginTransaction();
            database.execSQL("UPDATE " + BrelokAuthorisations.authorisations + " SET " + BrelokAuthorisations.colIsLogined + " =0, " + BrelokAuthorisations.colSessionId
                    + "= '' WHERE " + BrelokAuthorisations.colLogin + "='" + user.login + "'", new Object[]{});
            database.setTransactionSuccessful();
        }finally {
            database.endTransaction();
        }
    }

    /**
     * Вставляет в таблицу запись о новом пользователе
     * @param user
     */
    public void insertUser(BrelokUserInfo user){
       try{
           database.beginTransaction();
           ContentValues cv = new ContentValues();
           cv.put(BrelokAuthorisations.colLogin,user.login);
           cv.put(BrelokAuthorisations.colPassword,user.pass);
           cv.put(BrelokAuthorisations.colSessionId,user.sessionId);
           cv.put(BrelokAuthorisations.colIsLogined,1);
           database.insert(BrelokAuthorisations.authorisations,BrelokAuthorisations.colID,cv);
           database.setTransactionSuccessful();
       }finally {
           database.endTransaction();
       }
    }

    /**
     * пишет sessionId
     * @param user
     */
    public void writeSessionId(BrelokUserInfo user){
        // UPDATE BrelokAuthorisations SET BrelokAuthorisations.SessionId='' WHERE BrelokAuthorisations.Login=''
        try {
            database.beginTransaction();
            database.execSQL("UPDATE " + BrelokAuthorisations.authorisations + " SET " + BrelokAuthorisations.colSessionId + " ='"+user.sessionId+"'"
                    +" WHERE "+BrelokAuthorisations.colLogin+" = '"+user.login+"'", new Object[]{});
            database.setTransactionSuccessful();
        }finally {
            database.endTransaction();
        }
    }

    public void setLogedIn(BrelokUserInfo user, boolean onShip){
        // UPDATE BrelokAuthorisations SET BrelokAuthorisations.IsLogined=1 WHERE BrelokAuthorisations.Login=''
        String s;
        if (onShip){
            s=", isOnAShip=1";
        }else {
            s = ", isOnAShip=0";
        }
        try{
            database.beginTransaction();
            database.execSQL("UPDATE " + BrelokAuthorisations.authorisations + " SET " + BrelokAuthorisations.colIsLogined + " =1"+s+" WHERE "+BrelokAuthorisations.colLogin+" = '"+user.login+"'", new Object[]{});
            database.setTransactionSuccessful();
        }finally {
            database.endTransaction();
        }
    }
    //endregion

    /**
     * проверяет, нет ли уже в бд такого юзера
     * @param user
     * @return
     */
    public boolean isUserExist(BrelokUserInfo user){
        // SELECT _id FROM BrelokAuthorisations WHERE Login=''
        Cursor cursor;
        try{
            database.beginTransaction();
            cursor=database.rawQuery("SELECT _id FROM " + BrelokAuthorisations.authorisations + " WHERE " + BrelokAuthorisations.colLogin + " = '" + user.login + "'", null);
            database.setTransactionSuccessful();
        }finally {
            database.endTransaction();
        }
        cursor.getCount();
        if (cursor.getCount()==0){
            return false;
        }
        else return true;
    }

    /**
     * если уже был залогинен, то не нужно снова запускать окно входа
     * @return
     */
    public boolean ifUserLoggined(){
        // TODO написать sql запрос текстом
        Cursor cursor;
        try{
            database.beginTransaction();
            cursor=database.rawQuery("SELECT "+BrelokAuthorisations.colIsLogined+" FROM "+BrelokAuthorisations.authorisations+" WHERE "+BrelokAuthorisations.colIsLogined+" = 1",null);
            database.setTransactionSuccessful();
        }finally {
            database.endTransaction();
        }
        if (cursor.getCount()>0){
            cursor.moveToFirst();
            if (cursor.getInt(0)==1) {
                return true;
            }
            else return false;
        }else return false;
    }

    /**
     * возвращает текущего залогиненого пользователя
     * @return
     */
    public BrelokUserInfo getLogginedUser(){
        // SELECT Login FROM BrelokAuthorisations WHERE IsLogined=1
        Cursor cursor;
        try{
            database.beginTransaction();
            cursor=database.rawQuery("SELECT *"+" FROM "+BrelokAuthorisations.authorisations+" WHERE "+BrelokAuthorisations.colIsLogined+"=1",null);
            database.setTransactionSuccessful();

        }finally {
            database.endTransaction();
        }

        if (cursor.getCount()>0){
            cursor.moveToFirst();  // идем в начало массива

            return new BrelokUserInfo(cursor.getString(cursor.getColumnIndex(BrelokAuthorisations.colLogin))
                    ,cursor.getString(cursor.getColumnIndex(BrelokAuthorisations.colPassword))
                    ,cursor.getString(cursor.getColumnIndex(BrelokAuthorisations.colSessionId)));
        }
        return new BrelokUserInfo("","","");
    }

    public String getSessionID(BrelokUserInfo user){
        // SELECT BrelokAuthorisations.SessionId FROM BrelokAuthorisations WHERE (Login = '' AND Password = '')
        Cursor cursor;
        try{
            database.beginTransaction();
           cursor=database.rawQuery("SELECT "+BrelokAuthorisations.authorisations+"."+BrelokAuthorisations.colSessionId+" FROM "+ BrelokAuthorisations.authorisations+" WHERE ("
                    +BrelokAuthorisations.colLogin+" = '"+user.login+"' AND "+BrelokAuthorisations.colPassword+" = '"+user.pass+"' )",null );
            database.setTransactionSuccessful();
        }finally {
            database.endTransaction();
        }
        //TODO разобраться, почему работает с такими костылями
        if (cursor.getCount()>0){
            cursor.moveToFirst();  // идем в начало массива
           //string a= cursor.getString(cursor.getColumnIndex(BrelokAuthorisations.colSessionId));
            //return cursor.getString(cursor.getColumnIndex("SessionId"));
            return cursor.getString(0); // через getIndex неработаеот, приходит null
        }
        else return "";
    }

    public boolean getOnShipState(String login) {
         //SELECT BrelokAuthorisations.isOnAShip FROM BrelokAuthorisations WHERE BrelokAuthorisations.Login='admin'
        Cursor cursor;
        boolean onBoard = false;
        try{
            database.beginTransaction();
            cursor =  database.rawQuery("SELECT "+BrelokAuthorisations.colIsOnAShip+" FROM "+BrelokAuthorisations.authorisations+" WHERE "+BrelokAuthorisations.colLogin+" = '"+login+"'",null);
            database.setTransactionSuccessful();
        }finally {
            database.endTransaction();
        }
        if (cursor.getCount()>0){
            cursor.moveToFirst();
            if(cursor.getInt(cursor.getColumnIndex(BrelokAuthorisations.colIsOnAShip))==1){
                onBoard = true;
            } else{
                onBoard = false;
            }
        }
        return onBoard;
    }
    //endregion

    //region Global methods

    /**
     * чистит таблицу полностью, тк truncate нет
     * @param tableName
     */
    public void clearTable(String tableName){
        // DELETE FROM
        try{
            database.beginTransaction();
            database.execSQL("DELETE FROM "+tableName);
            database.setTransactionSuccessful();
        }   finally {
            database.endTransaction();
        }
    }

    /**
     * возвращает id последней записи в таблице
     * @param tableName
     * @param idColName
     * @return
     */
    public int getLastID(String tableName, String idColName){
        // SELECT _id FROM BrelokCoordinatesArchive WHERE _id =(SELECT MAX(_id) FROM BrelokCoordinatesArchive))
        // TODO добавить в сборщик запроса переменную с название поля
        Cursor cursor;
        try{
            database.beginTransaction();
            cursor=database.rawQuery("SELECT "+idColName +" FROM "+tableName+" WHERE "+idColName+" =(SELECT MAX("+idColName+") FROM "+tableName+" )",null);
            database.setTransactionSuccessful();
        }finally {
            database.endTransaction();
        }
        if(cursor.getCount()>0){
            cursor.moveToFirst();
            return cursor.getInt(0);
        }
        return 0;
    }

    //endregion
}
