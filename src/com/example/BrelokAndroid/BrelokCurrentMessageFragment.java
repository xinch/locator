package com.example.BrelokAndroid;

import android.content.*;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;
import com.example.BrelokAndroid.BrelokDataStructures.BrelokListItem;
import com.example.BrelokAndroid.BrelokDataStructures.BrelokMsgLocation;
import com.example.BrelokAndroid.BrelokDataStructures.BrelokMyLocation;
import com.example.BrelokAndroid.DBTables.BrelokCoordinatesReceived;
import com.example.BrelokAndroid.DBTables.BrelokMessagesArchive;

import java.util.ArrayList;

/**
 * Created with IntelliJ IDEA.
 * User: Admin
 * Date: 01.10.13
 * Time: 12:16
 * To change this template use File | Settings | File Templates.
 */
public class BrelokCurrentMessageFragment extends ListFragment implements  BrelokMessagesArchive{

    private  BrelokDbAdapter  dbAdapter;
    private BroadcastReceiver broadcastReceiver;
    private Cursor cursor;
    BrelokCurrentMessagesAdapter adapter;
    private BrelokListItem brelokListItem;

    public BrelokCurrentMessageFragment(){

    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void onDestroy(){
        super.onDestroy();
        if (dbAdapter!=null){
            dbAdapter.closeHelper();
        }
    }

    @Override
    public void onPause(){
        super.onPause();
        getActivity().unregisterReceiver(broadcastReceiver);
    }

    @Override
    public void onResume(){
        super.onResume();
        listUpdate();
        IntentFilter intentFilter=new IntentFilter("android.intent.action.MAIN");
        broadcastReceiver =new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                if(intent.getIntExtra("task",1)==2){   //пришла комадна обновиться
                    listUpdate();
                }
            }
        };
        getActivity().registerReceiver(this.broadcastReceiver, intentFilter);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState){
        View rootView = inflater.inflate(R.layout.fragment_current_messages, container, false);

        dbAdapter= new BrelokDbAdapter(getActivity());
        listUpdate();
        return rootView;
    }

    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        super.onListItemClick(l, v, position, id);
        brelokListItem = adapter.getClickedItem(position);
        dbAdapter.openHelper();
        dbAdapter.setMessageRead(brelokListItem.getId());
        //создание окна подробностей сообщейния
        Intent intent = new Intent(getActivity(),BrelokMessageScreen.class);
        intent.putExtra("id",brelokListItem.getId());
        dbAdapter.insertQuerry(BrelokMessagesArchive.messagesArchive,brelokListItem.getId());     //пишем в очередь id шники прочтанных сообщений
        startActivity(intent);
        listUpdate();
    }

    /***
     * тянет из базы непрочитанные сообщения и кормит их listview
     */
    public  void listUpdate(){
        dbAdapter.openHelper();
        cursor = dbAdapter.getAllUnreadedMessages();
        ArrayList<BrelokListItem> res = new ArrayList<BrelokListItem>();
        if(cursor != null)
        {
            if (cursor.moveToFirst())
            {
                do{
                    brelokListItem = new BrelokListItem(cursor.getInt(cursor.getColumnIndex(BrelokMessagesArchive.colMessageID))
                            , cursor.getInt(cursor.getColumnIndex(BrelokMessagesArchive.colReaded))
                            , cursor.getString(cursor.getColumnIndex(BrelokMessagesArchive.colText))
                            //, cursor.getString(cursor.getColumnIndex(BrelokMessagesArchive.colCreateTime)).replaceAll("T"," "));
                            ,cursor.getString(cursor.getColumnIndex(BrelokMessagesArchive.colCreateTime)));
                    if(cursor.getString(cursor.getColumnIndex(BrelokMessagesArchive.colCoordinates)) != null){  //если есть координаты
                        BrelokMsgLocation bml  = new BrelokMsgLocation(cursor.getDouble(cursor.getColumnIndex(BrelokCoordinatesReceived.colLatitude))
                                ,cursor.getDouble(cursor.getColumnIndex(BrelokCoordinatesReceived.colLongitude)));
                        brelokListItem.setLocation(bml);
                    }
                     res.add(brelokListItem);
                }while (cursor.moveToNext());
            }
        }
        adapter = new BrelokCurrentMessagesAdapter(getActivity(), res);
        setListAdapter(adapter);
        dbAdapter.closeHelper();
    }
}