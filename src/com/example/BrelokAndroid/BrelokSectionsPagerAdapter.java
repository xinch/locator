package com.example.BrelokAndroid;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import java.util.Locale;




/**
 * Created with IntelliJ IDEA.
 * User: Admin
 * Date: 07.10.13
 * Time: 8:34
 * To change this template use File | Settings | File Templates.
 */

/**
 * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
 * one of the sections/tabs/pages.
 */
public class BrelokSectionsPagerAdapter extends FragmentPagerAdapter {
    public BrelokSectionsPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        switch (position){
            case 0:{
                Fragment fragment = new BrelokCurrentMessageFragment();
                return  fragment;
            }
            case 1:   {
                Fragment fragment=new BrelokMapFragment();
                return fragment;
            }
            case 2:   {
                Fragment fragment=new BrelokMessagesArchiveFragment();
                return fragment;
            }
            default: {
                Fragment fragment = new BrelokCurrentMessageFragment();
                return fragment;
            }
        }
    }

    @Override
    public int getCount() {
        // Show 3 total pages.
        return 3;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        Locale l = Locale.getDefault();
        switch (position) {
            case 0:
                return BrelokMainActivity.contextActivity.getString(R.string.title_section1).toUpperCase(l);
            case 1:
                return BrelokMainActivity.contextActivity.getString(R.string.title_section2).toUpperCase(l);
            case 2:
                return BrelokMainActivity.contextActivity.getString(R.string.title_section3).toUpperCase(l);
        }
        return null;
    }
}
