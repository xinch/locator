package com.example.BrelokAndroid;

import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;
import com.example.BrelokAndroid.DBTables.BrelokMessagesArchive;

/**
 * Created with IntelliJ IDEA.
 * User: Admin
 * Date: 31.10.13
 * Time: 16:55
 * To change this template use File | Settings | File Templates.
 */
public class BrelokMessageContentProvider extends ContentProvider implements BrelokMessagesArchive{


    static final String PROVIDER_NAME = "com.example.provider.BrelokMobileData";

    static final String MESSAGE_PATH = BrelokMessagesArchive.messagesArchive;

    public static final Uri CONTENT_URI=Uri.parse("content://"+PROVIDER_NAME+"/"+MESSAGE_PATH);
    BrelokDbAdapter dbAdapter;
    static final int MESSAGE=1;
    static final int MESSAGE_ID=2;

    private static final UriMatcher uriMatcher;
    static {
        uriMatcher=new UriMatcher(UriMatcher.NO_MATCH);
        uriMatcher.addURI(PROVIDER_NAME,BrelokMessagesArchive.messagesArchive,MESSAGE);
        uriMatcher.addURI(PROVIDER_NAME,BrelokMessagesArchive.messagesArchive+"/#",MESSAGE_ID);
    }

    @Override
    public boolean onCreate() {
        dbAdapter=new BrelokDbAdapter(getContext());
        dbAdapter.openHelper();
        return (dbAdapter==null)? false:true;
    }

    @Override
    public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {
        SQLiteQueryBuilder queryBuilder=new SQLiteQueryBuilder();
        queryBuilder.setTables(BrelokMessagesArchive.messagesArchive);

        switch (uriMatcher.match(uri)){
            case MESSAGE:

        }

        Cursor cursor;
        cursor = dbAdapter.getAllCurrentMessages();
        return cursor;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public String getType(Uri uri) {

        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public Uri insert(Uri uri, ContentValues values) {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs) {
        return 0;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public int update(Uri uri, ContentValues values, String selection, String[] selectionArgs) {
        return 0;  //To change body of implemented methods use File | Settings | File Templates.
    }
}
