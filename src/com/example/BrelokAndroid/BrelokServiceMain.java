package com.example.BrelokAndroid;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.*;

import android.os.Message;
import com.example.BrelokAndroid.AsyncTasks.*;
import com.example.BrelokAndroid.BaseMobile.*;
import com.example.BrelokAndroid.BrelokConverters.BrelokDateConvertor;
import com.example.BrelokAndroid.BrelokDataStructures.*;
import com.example.BrelokAndroid.DBTables.BrelokCoordinatesArchive;

import android.util.Log;
import com.example.BrelokAndroid.DBTables.BrelokMessagesArchive;

import java.util.Date;
import java.util.concurrent.ExecutionException;


public class BrelokServiceMain extends Service implements BrelokCoordinatesArchive, BrelokMainSettings {

    private LocationManager mLocManager;
    private BrelokDbAdapter mDBAdapter;
    private LocationListener mLocListener;
    //private Thread mThreadCoordInsert;
    //private Thread mServerExchange;
    private Boolean mStop;
    private BrelokDateConvertor dateConvertor;
    private String provider;
    private Handler handler; // нужно, чтобы передавать из потока в главный сообщения
    private int isLocStop=1;

    private static final int NOTIFY_ID = 1;

    //region Override methods
    @Override
    public void onDestroy(){
        mStop=true;
        mLocManager.removeUpdates(mLocListener);
        super.onDestroy();
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }

    public void onCreate(){
        super.onCreate();
    }

    //endregion

    public int onStartCommand(Intent intent,int flags, int startId){
        mDBAdapter=new BrelokDbAdapter(getApplicationContext());
        mStop=false;
        //TODO Исправить костыль с handler
        handler=new Handler(){
          public void handleMessage(Message msg){
              switch (msg.what){
                  case RESUME_LOCATION:{
                      locationResume();
                      break;
                  }
                  case PAUSE_LOCATION:{
                      locationStop();
                      break;
                  }
                  case UPDATE_PROVIDER:{
                      updateProveder();
                      break;
                  }
              }
          }
        } ;
        // запускаем потоки обмена  с сервером и записи местоположения
        locationUpdatesInsert();
        locationTimer();
        serviceExchangeTimer();
        return START_STICKY;
    }

    //region Thread timers
    /**
     * метод создает поток, который паузится на заданное время. Типа таймер
     */
    private void locationTimer(){
       Thread mThreadCoordInsert = new  Thread()
        {
            @Override
            public void run(){
                try{
                    while(!mStop){
                        handler.sendEmptyMessage(UPDATE_PROVIDER);
                        handler.sendEmptyMessage(RESUME_LOCATION);  // запускаем локацию
                        sleep(SLEEP_TIME_GPS * 1000);
                        Log.d("BrelokAA", "Tiks ");
                        handler.sendEmptyMessage(PAUSE_LOCATION);
                    }
                } catch (InterruptedException e) {
                    e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                }
            }
        };
        mThreadCoordInsert.start();
    }

    /***
     * таймер обмена с сервером
     */
    private void serviceExchangeTimer(){
       Thread mServerExchange = new  Thread(){
           @Override
        public void run(){
               try{
                   while(!mStop){
                       serviceExchange();
                       sleep(SLEEP_TIME_SERVICE*1000);
                 // TODO запуск обмена с сервисом

                   }
               } catch (InterruptedException e){
                   e.printStackTrace();
               }
           }
        };
        mServerExchange.start();
    }
    //endregion

    /**
     * изначальная инициализация gps
     */
    private void locationUpdatesInsert(){
        if (mLocManager ==null){
            mLocManager = (LocationManager) getApplicationContext().getSystemService(Context.LOCATION_SERVICE);
        }
        provider = mLocManager.getBestProvider(new Criteria(), true);
        mLocListener = new LocationListener() {
            //region Override methods
            // TODO елси по гпс координат нет, переходим на координаты от сотовых вышек
            @Override
            public void onLocationChanged(Location location) {   // если тюкаем гпс, то точность выше
                if (provider.equals(LocationManager.GPS_PROVIDER)){
                    if (location.getAccuracy() <= GPS_ACCURACY){
                        mDBAdapter.openHelper();
                        dateConvertor = new BrelokDateConvertor();
                        String date = dateConvertor.toSQLDATETIME(new Date(location.getTime()));  // парсим дату в формат DATETIME SQL
                        mDBAdapter.insertCoordinates(location,date);
                        int id=mDBAdapter.getLastID(BrelokCoordinatesArchive.coordinatesArchive,BrelokCoordinatesArchive.colLocationID);
                        mDBAdapter.insertQuerry(BrelokCoordinatesArchive.coordinatesArchive,id);
                        //mDBAdapter.closeHelper();
                        sendBroadcastMsg(1);  // шлем сообщение , что координаты обновились
                        locationStop();
                    }
                } else {
                    if (provider.equals(LocationManager.NETWORK_PROVIDER)){     // если тюкаем не гпс, то точность ниже
                      if(location.getAccuracy()<= NETWORK_ACCURACY){
                          mDBAdapter.openHelper();
                          dateConvertor = new BrelokDateConvertor();
                          String date = dateConvertor.toSQLDATETIME(new Date(location.getTime()));  // парсим дату в формат DATETIME SQL
                          mDBAdapter.insertCoordinates(location,date);
                          int id=mDBAdapter.getLastID(BrelokCoordinatesArchive.coordinatesArchive,BrelokCoordinatesArchive.colLocationID);
                          mDBAdapter.insertQuerry(BrelokCoordinatesArchive.coordinatesArchive,id);
                          //mDBAdapter.closeHelper();
                          sendBroadcastMsg(1);  // шлем сообщение , что координаты обновились
                          locationStop();
                      }
                    }  else if(!((provider.equals(LocationManager.NETWORK_PROVIDER)) || (provider.equals(LocationManager.GPS_PROVIDER)))){
                        ;//TODO Сделать уведолмение о невозможности навигации
                }
                }
            }
            //region Not uses
            @Override
            public void onStatusChanged(String provider, int status, Bundle extras) {
                //To change body of implemented methods use File | Settings | File Templates.
            }

            @Override
            public void onProviderEnabled(String provider) {
                //To change body of implemented methods use File | Settings | File Templates.
            }

            @Override
            public void onProviderDisabled(String provider) {
                //To change body of implemented methods use File | Settings | File Templates.
            }
            //endregion
            //endregion
        };
    }

    /***
     * метод отправляет широковещательное сообщение о наличии новых координат
     */
    private void sendBroadcastMsg(int task){
        Intent intent=new Intent("android.intent.action.MAIN").putExtra("task",task);
        this.sendBroadcast(intent);
    }

    private void locationStop(){
        if(isLocStop==0){
            mLocManager.removeUpdates(mLocListener);
            isLocStop=1;
            Log.d("BrelokAA", "Stop ");
        }
    };

    private void locationResume(){
        if (isLocStop==1){
            isLocStop=0;
            Log.d("BrelokAA", "Start ");
            if(provider!=null) {
                mLocManager.requestLocationUpdates(provider, 1000, 0, mLocListener);
            }
        }
    };

    private void updateProveder(){
        provider = mLocManager.getBestProvider(new Criteria(), true);
    }

    //region Service exchange methods

    /***
     * изначальный пинок сервиса, запускает методы получения/отправки
     */
    private void serviceExchange(){
        mDBAdapter.openHelper();
        BrelokUserInfo user;
        user = mDBAdapter.getLogginedUser();
       // mDBAdapter.closeHelper();
        //проверка CheckState
       // LoginAnswer loginAnswer;
      //  CheckStateTask t = new CheckStateTask();
       // t.execute(user);
        if(checkState()){
            getMessages();
            sendLocalCoordinates();
            sendReadedMsgId();
        }
    }

    private void sendLocalCoordinates(){
        mDBAdapter.openHelper();
        Cursor cursor = mDBAdapter.getCoordinatesToSend();
        BrelokUserInfo brelokUserInfo;
        VectorGisPoint points = DbToVectorGisPoints(cursor);
        cursor.close();
       //mDBAdapter.closeHelper();

        //mDBAdapter.openHelper();
        brelokUserInfo = mDBAdapter.getLogginedUser();
        brelokUserInfo.sessionId = mDBAdapter.getSessionID(brelokUserInfo);
        //mDBAdapter.closeHelper();

        boolean isOnShip = mDBAdapter.getOnShipState(brelokUserInfo.login);

        BrelokDateConvertor dc = new BrelokDateConvertor();
        BrelokMyLocation brelokMyLocation = new BrelokMyLocation(points,brelokUserInfo.sessionId,dc.toSQLDATETIME(new Date()),isOnShip);

        //отправка координат
        SendSelfPointsTask t = new SendSelfPointsTask();
        LoginAnswer loginAnswer;
        t.execute(brelokMyLocation);
        try {
            loginAnswer = t.get();
            if (loginAnswer !=null){

            }
        } catch (InterruptedException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        } catch (ExecutionException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }

       // mDBAdapter.openHelper();
        mDBAdapter.clearQuerry(BrelokCoordinatesArchive.coordinatesArchive);   //чистим очередь
        mDBAdapter.clearMyCoordinates();    //чистим свои координаты
        mDBAdapter.closeHelper();
    }

    /***
     * метод тянет из сервиса сообщения с последней даты и передает вектор на парсинг в базу
     */
    private void getMessages(){
        GetMessageAnswer messageAnswer;
        BrelokUserInfo brelokUserInfo;
        BrelokGetMessage brelokGetMessage;
        String lastDate;

        mDBAdapter.openHelper();
        brelokUserInfo = mDBAdapter.getLogginedUser();
        brelokUserInfo.sessionId = mDBAdapter.getSessionID(brelokUserInfo);
        lastDate = mDBAdapter.getLastDateMessage();
        mDBAdapter.closeHelper();
        if (lastDate == null){
            lastDate="1900-01-01T00:00:00";
        }
        brelokGetMessage = new BrelokGetMessage(lastDate,brelokUserInfo.sessionId);
        GetMessagesTask t = new GetMessagesTask();
        t.execute(brelokGetMessage);
        try {
            messageAnswer = t.get();
            vectorMessagesToDB(messageAnswer); // отдаем вектор парсеру и писателю в бд
        } catch (InterruptedException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        } catch (ExecutionException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
    }

    /***
     * шлет id прочитанных сообщений
     */
    private void sendReadedMsgId(){
        BrelokSendReadedMsgId brelokSendReadedMsgId;
        SetMessageReadAnswer setMessageReadAnswer;

        mDBAdapter.openHelper();
        Cursor cursor = mDBAdapter.getReadedMessagesId();

        BrelokUserInfo brelokUserInfo;
        brelokUserInfo = mDBAdapter.getLogginedUser();
        brelokUserInfo.sessionId = mDBAdapter.getSessionID(brelokUserInfo);


        brelokSendReadedMsgId = new BrelokSendReadedMsgId(DbToInt64Ids(cursor),brelokUserInfo.sessionId);
        SetMessagesAsReadTask t = new SetMessagesAsReadTask();

        t.execute(brelokSendReadedMsgId);
        try{
            setMessageReadAnswer = t.get();

        }catch (InterruptedException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        } catch (ExecutionException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }

        mDBAdapter.clearQuerry(BrelokMessagesArchive.messagesArchive);   //чистим очередь
    }
    //endregion

    /***
     * парсит вектор с сообщениями и пишет их в бд
     * @param messageAnswer
     */
    private void vectorMessagesToDB(GetMessageAnswer messageAnswer){
        int achtung=0; // контроль важности сообщений

         if(messageAnswer!=null){
             notifySystemTray(messageAnswer.messages.size());
             if (messageAnswer.lA.isLoggedIn && messageAnswer.messages.size()!=0){
                 for (int i=0;i<messageAnswer.messages.size();i++){
                     //запонляем объект сообщения для записи в бд
                     int lastId=0;
                     BrelokMessageReceived brelokMessageReceived = new BrelokMessageReceived();
                     brelokMessageReceived.exID = messageAnswer.messages.elementAt(i).id;
                     brelokMessageReceived.createTime = messageAnswer.messages.elementAt(i).dateCreate;
                     brelokMessageReceived.warningLevel = messageAnswer.messages.elementAt(i).priority;
                     if (brelokMessageReceived.warningLevel>3){
                         //TODO будим приложение, рисуем активити с сообщениями
                     }
                     brelokMessageReceived.Text = messageAnswer.messages.elementAt(i).msg;
                     if (messageAnswer.messages.elementAt(i).point!=null){  // если есть координаты. то пишем и их
                         if(messageAnswer.messages.elementAt(i).point.latitude!=0 && messageAnswer.messages.elementAt(i).point.longitude!=0){
                             BrelokMsgLocation location = new BrelokMsgLocation(messageAnswer.messages.elementAt(i).point.latitude,messageAnswer.messages.elementAt(i).point.longitude);
                             brelokMessageReceived.Coordinates = location;
                         }
                     }
                     mDBAdapter.openHelper();
                     mDBAdapter.insertMessage(brelokMessageReceived);
                     mDBAdapter.closeHelper();
                 }
         }
            sendBroadcastMsg(2);  //говорим листу, что пора обновится
            if (achtung==5){
                //TODO Приложение должно развернуться
            }
            // сообщение в приложение что пришли сообщения
            //TODO notification в системный трей
        }
    }

    private VectorGisPoint DbToVectorGisPoints(Cursor cursor){
        VectorGisPoint points = new VectorGisPoint();
        if(cursor.moveToFirst()){
            do{
                GisPoint gisPoint = new GisPoint();
                gisPoint.latitude = cursor.getDouble(cursor.getColumnIndex(BrelokCoordinatesArchive.colLatitude));
                gisPoint.longitude = cursor.getDouble(cursor.getColumnIndex(BrelokCoordinatesArchive.colLongitude));
                gisPoint.accuracy = cursor.getDouble(cursor.getColumnIndex(BrelokCoordinatesArchive.colAccurancy));
                gisPoint.altitude = cursor.getDouble(cursor.getColumnIndex(BrelokCoordinatesArchive.colAltitude));
                gisPoint.bearing = cursor.getDouble(cursor.getColumnIndex(BrelokCoordinatesArchive.colBearing));
                gisPoint.speed = cursor.getDouble(cursor.getColumnIndex(BrelokCoordinatesArchive.colSpeed));
                gisPoint.timeStamp = cursor.getString(cursor.getColumnIndex(BrelokCoordinatesArchive.colDateTtime));

                gisPoint.accuracySpecified = true;
                gisPoint.altitudeSpecified = true;
                gisPoint.bearingSpecified = true;
                gisPoint.latitudeSpecified = true;
                gisPoint.longitudeSpecified = true;
                gisPoint.speedSpecified = true;
                gisPoint.timeStampSpecified = true;
                points.add(gisPoint);
            }while (cursor.moveToNext());
        }
        return points;
    }

    private VectorInt64 DbToInt64Ids (Cursor cursor){
        VectorInt64 ids = new VectorInt64();
        if(cursor.moveToFirst()){
            do{
                ids.add(cursor.getInt(cursor.getColumnIndex(BrelokMessagesArchive.colExternalID)));
            }while (cursor.moveToNext());
        }
        return ids;
    }

    /***
     * метод шлет в обратку ид только что полученных сообщений
     * @param messageAnswer
     */
    private void sendVerificate(GetMessageAnswer messageAnswer){
        if (checkState()){
            VectorInt64 ids=new VectorInt64();
            for(int i=0;i<messageAnswer.messages.size();i++){
                ids.add(messageAnswer.messages.elementAt(i).id);  // заталкиваем в вектор ид принятых сообщений
            }
            BrelokGetMessagesByID brelokGetMessagesByID = new BrelokGetMessagesByID(ids,messageAnswer.lA.sessionId); // объект для передачи парамтеров в async task

            GetMessageByIdTask t = new GetMessageByIdTask();
            t.execute(brelokGetMessagesByID);
        }
    }

    /***
     * метод свотрит, залогигены ли мы. Ели нет, то логинится заново
     */
    private boolean checkState(){
        boolean logins = false;

        mDBAdapter.openHelper();
        BrelokUserInfo user;
        user = mDBAdapter.getLogginedUser();
       // mDBAdapter.closeHelper();
        //проверка CheckState
        LoginAnswer loginAnswer;
        CheckStateTask t = new CheckStateTask();
        t.execute(user);
        try{
            loginAnswer = t.get();
            if(loginAnswer != null){
                if(!loginAnswer.isLoggedIn){ // опа, а уже и не залогинены
                    LoginTask lt= new LoginTask();
                    lt.execute(user);
                    //TODO
                    user.sessionId = lt.get().sessionId;     //получаем нывой  sessionId
                   // mDBAdapter.openHelper();
                    mDBAdapter.writeSessionId(user); // переписываем sessionId
                    mDBAdapter.closeHelper();
                    logins = true; //сервис жив
                }  else{
                    logins = true;
                }
            } else{
                logins = false; // сервис умер
                //TODO сообщение, что сервис тютю
            }
        } catch (InterruptedException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        } catch (ExecutionException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
        return logins;
    }

    /***
     * запускает активити из сервиса, если приоритет 4 и выше
     */
    private void startIfAchtung(){
        Intent i = new Intent();
        i.setClass(this,BrelokMainActivity.class);
        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(i);
    }

    private void notifySystemTray(int msgCount){

        //BrelokNotifications brelokNotifications = new BrelokNotifications();
       // brelokNotifications.startActivity(new Intent(getApplicationContext(), BrelokMainActivity.class));
        if(msgCount>0){

        }
       }
}