package com.example.BrelokAndroid;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.database.Cursor;
import android.support.v4.app.ListFragment;
import android.widget.*;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.example.BrelokAndroid.BrelokConverters.BrelokDateConvertor;
import com.example.BrelokAndroid.BrelokDataStructures.BrelokListItem;
import com.example.BrelokAndroid.BrelokDataStructures.BrelokMsgLocation;
import com.example.BrelokAndroid.DBTables.*;

import java.security.PrivateKey;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

/**
 * Created with IntelliJ IDEA.
 * User: Admin
 * Date: 23.09.13
 * Time: 13:14
 * To change this template use File | Settings | File Templates.
 */
public class BrelokMessagesArchiveFragment extends ListFragment implements BrelokCoordinatesArchive {

    private  BrelokDbAdapter  dbAdapter;
    private  ListView messages;
    private TextView textViewStart;
    private TextView textViewEnd;
    private Button btnShowArchive;
    private Cursor cursor;
    private BrelokListItem brelokListItem;
    private BrelokCurrentMessagesAdapter adapter;

    private String Start = "1-1-2013";
    private String End = "1-1-2014";

public BrelokMessagesArchiveFragment(){

}
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState){
        View rootView = inflater.inflate(R.layout.fragment_archive_messages, container, false);
        messages = (ListView) rootView.findViewById(R.id.lst_archive_messages);
        dbAdapter=new BrelokDbAdapter(getActivity());
        textViewStart = (TextView) rootView.findViewById(R.id.txtDatePickerStart);
        textViewEnd = (TextView) rootView.findViewById(R.id.txtDatePickerEnd);
        btnShowArchive = (Button) rootView.findViewById(R.id.btnShowArchive);

        textViewStart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDatePicker(1);
            }
        });
        textViewEnd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDatePicker(2);
            }
        });

        btnShowArchive.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               // показать архив
                getMessageByDate(textViewStart.getText().toString(), textViewEnd.getText().toString());
                Start = textViewStart.getText().toString();
                End = textViewEnd.getText().toString();
            }
        });
        textViewStart.setText(Start);
        textViewEnd.setText(End);

        return rootView;
    }

    @Override
    public void onListItemClick(ListView l, View v, int position, long id){
        super.onListItemClick(l,v,position,id);

        brelokListItem = adapter.getClockedItemForArchive(position);
        //создание окна подробностей сообщейния
        Intent intent = new Intent(getActivity(),BrelokMessageScreen.class);
        intent.putExtra("id",brelokListItem.getId());
        startActivity(intent);
    }

    public  void getMessageByDate(String dateFrom, String dateTo){
        dbAdapter.openHelper();
        cursor = dbAdapter.getAllReadedMessages();
        BrelokDateConvertor bdc = new BrelokDateConvertor();
        ArrayList<BrelokListItem> res = new ArrayList<BrelokListItem>();
        if(cursor != null)
        {
            if (cursor.moveToFirst())
            {
                do{
                    String s = cursor.getString(cursor.getColumnIndex(BrelokMessagesArchive.colCreateTime));
                    if((bdc.SQLDATETIMEtoAndroid(s)).compareTo(getDate(dateFrom))>0
                            && bdc.SQLDATETIMEtoAndroid(s).compareTo(getDate(dateTo))<0){
                        brelokListItem = new BrelokListItem(cursor.getInt(cursor.getColumnIndex(BrelokMessagesArchive.colMessageID))
                                , cursor.getInt(cursor.getColumnIndex(BrelokMessagesArchive.colReaded))
                                , cursor.getString(cursor.getColumnIndex(BrelokMessagesArchive.colText))
                                , cursor.getString(cursor.getColumnIndex(BrelokMessagesArchive.colCreateTime)));
                        if(cursor.getString(cursor.getColumnIndex(BrelokMessagesArchive.colCoordinates)) != null){  //если есть координаты
                            BrelokMsgLocation bml  = new BrelokMsgLocation(cursor.getDouble(cursor.getColumnIndex(BrelokCoordinatesReceived.colLatitude))
                                    ,cursor.getDouble(cursor.getColumnIndex(BrelokCoordinatesReceived.colLongitude)));
                            brelokListItem.setLocation(bml);
                        }
                        res.add(brelokListItem);
                    }
                }while (cursor.moveToNext());
            }
        }
        adapter = new BrelokCurrentMessagesAdapter(getActivity(), res);
        setListAdapter(adapter);
        dbAdapter.closeHelper();
    }

    public Date getDate(String _date){
        SimpleDateFormat sdf = new SimpleDateFormat("dd-mm-yyyy");
        //_date=_date.replaceAll("/","-");
        Date date = null;
        try {
            date = sdf.parse(_date);
        } catch (ParseException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
        return date;
    }

    @Override
    public void onDestroy(){
        super.onDestroy();
        if (dbAdapter!=null){
            dbAdapter.closeHelper();
        }
    }

    @Override
    public void onPause(){
        super.onPause();
        //  getActivity().unregisterReceiver(broadcastReceiver);
    }

    @Override
    public void onResume(){
        super.onResume();
        getMessageByDate(textViewStart.getText().toString(),textViewEnd.getText().toString());
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState){
        super.onActivityCreated(savedInstanceState);
        this.setRetainInstance(true);
    }

    public void listupdate(){
        dbAdapter.openHelper();
        cursor = dbAdapter.getAllReadedMessages();
        ArrayList<BrelokListItem> res = new ArrayList<BrelokListItem>();
        BrelokDateConvertor bdc = new BrelokDateConvertor();
        if(cursor != null)
        {
            if (cursor.moveToFirst())
            {
                do{

                     brelokListItem = new BrelokListItem(cursor.getInt(cursor.getColumnIndex(BrelokMessagesArchive.colMessageID))
                            , cursor.getInt(cursor.getColumnIndex(BrelokMessagesArchive.colReaded))
                            , cursor.getString(cursor.getColumnIndex(BrelokMessagesArchive.colText))
                            , cursor.getString(cursor.getColumnIndex(BrelokMessagesArchive.colCreateTime)));
                    if(cursor.getString(cursor.getColumnIndex(BrelokMessagesArchive.colCoordinates)) != null){  //если есть координаты
                        BrelokMsgLocation bml  = new BrelokMsgLocation(cursor.getDouble(cursor.getColumnIndex(BrelokCoordinatesReceived.colLatitude))
                                ,cursor.getDouble(cursor.getColumnIndex(BrelokCoordinatesReceived.colLongitude)));
                        brelokListItem.setLocation(bml);
                    }
                    res.add(brelokListItem);
                }while (cursor.moveToNext());
            }
        }
        adapter = new BrelokCurrentMessagesAdapter(getActivity(), res);
        setListAdapter(adapter);
        dbAdapter.closeHelper();
    }

    // calendar внезапно считает месяцы с 0 а не с 1
    //region work DataPicker
    private void showDatePicker(int i) {
        BrelokDatePickerDialog date = new BrelokDatePickerDialog();

        switch (i){
            case 1:{
                /**
                 * Set Up Current Date Into dialog
                 */
                Calendar calender = Calendar.getInstance();
                Bundle args = new Bundle();
                String[] start = Start.split("-");
                args.putInt("year", Integer.valueOf(start[2]));
                int month =Integer.valueOf(start[1]); month--;
                args.putInt("month", month);
                args.putInt("day", Integer.valueOf(start[0]));
                date.setArguments(args);
                /**
                 * Set Call back to capture selected date
                 */
                date.setCallBack(ondateStart);
                date.show(getFragmentManager(), "Date Picker");
                break;
            }
            case 2:{
                /**
                 * Set Up Current Date Into dialog
                 */
                Calendar calender = Calendar.getInstance();
                Bundle args = new Bundle();
                String[] end = End.split("-");
                args.putInt("year", Integer.valueOf(end[2]));
                int month =Integer.valueOf(end[1]); month--;
                args.putInt("month", month);
                args.putInt("day", Integer.valueOf(end[0]));
                date.setArguments(args);
                /**
                 * Set Call back to capture selected date
                 */
                date.setCallBack(ondateEnd);
                date.show(getFragmentManager(), "Date Picker");
                break;
            }
    }
    }

    DatePickerDialog.OnDateSetListener ondateStart = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear,
                              int dayOfMonth) {
            ++monthOfYear;
            textViewStart.setText(dayOfMonth + "-" + monthOfYear + "-" + year);
        }
    };

    DatePickerDialog.OnDateSetListener ondateEnd = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear,
                              int dayOfMonth) {
            ++monthOfYear;
            textViewEnd.setText(dayOfMonth + "-" + monthOfYear + "-" + year);
        }
    };
    //endregion
}

