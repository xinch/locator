package com.example.BrelokAndroid.BrelokConverters;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created with IntelliJ IDEA.
 * User: Alexey
 * Date: 02.11.13
 * Time: 22:35
 * To change this template use File | Settings | File Templates.
 */
public class BrelokDateConvertor {

    public String toSQLDATETIME(Date date){
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd/HH:mm:ss");
        String sqlDate = sdf.format(date);
        sqlDate.replaceAll("/","T");
        return sqlDate.replaceAll("/","T");
    }

    public Date SQLDATETIMEtoAndroid(String _date){
        String s = _date.replaceAll("T"," ");
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-mm-dd");
        Date date=null;
        try {
             date = sdf.parse(s);
        } catch (ParseException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
        return date;
    }

    /***
     * метод конвертит дату в вид 1-1-2013
     * @param _date
     * @return
     */
    public String convertToShow(String _date){
        String[] temp = _date.split("T");
        String[] date = temp[0].split("-");
        String d = date[2]+"-"+date[1]+"-"+date[0]+" "+temp[1];
        return d;
    }
}
