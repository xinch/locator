package com.example.BrelokAndroid;

import java.util.Locale;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import com.example.BrelokAndroid.BrelokDataStructures.BrelokUserInfo;


public class BrelokMainActivity extends FragmentActivity {
    /**
     * The {@link android.support.v4.view.PagerAdapter} that will provide
     * fragments for each of the sections. We use a
     * {@link android.support.v4.app.FragmentPagerAdapter} derivative, which
     * will keep every loaded fragment in memory. If this becomes too memory
     * intensive, it may be best to switch to a
     * {@link android.support.v4.app.FragmentStatePagerAdapter}.
     */
    BrelokSectionsPagerAdapter mBrelokSetcionsPagerAdapter;

    /**
     * The {@link ViewPager} that will host the section contents.
     */
    public static Context contextActivity;
    private ViewPager mViewPager;

    //region Override region
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        contextActivity=this;
        startService(new Intent(this,BrelokServiceMain.class));
        mBrelokSetcionsPagerAdapter = new BrelokSectionsPagerAdapter(getSupportFragmentManager());
        mViewPager=(ViewPager) findViewById(R.id.pager);
        mViewPager.setAdapter(mBrelokSetcionsPagerAdapter);
    }

    @Override
    protected void onDestroy(){
        super.onDestroy();
        //stopService(new Intent(this,BrelokServiceMain.class));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item){
        if( item.getItemId()==R.id.action_logout){ //
           logout();
        }
        return super.onOptionsItemSelected(item);
    }
    //endregion

    /**
     * разлогин
     */
    private void logout(){
        BrelokDbAdapter dbAdapter=new BrelokDbAdapter(getApplicationContext());
        BrelokUserInfo user;
        dbAdapter.openHelper();
        user=dbAdapter.getLogginedUser();
        dbAdapter.logout(user);
        dbAdapter.closeHelper();
        stopService(new Intent(this,BrelokServiceMain.class));
        finish();
    }

}
