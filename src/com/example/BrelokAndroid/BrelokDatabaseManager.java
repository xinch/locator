package com.example.BrelokAndroid;

import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.concurrent.atomic.AtomicInteger;

/**
 * Created with IntelliJ IDEA.
 * User: Admin
 * Date: 09.12.13
 * Time: 10:40
 * To change this template use File | Settings | File Templates.
 */
public class BrelokDatabaseManager {
    private AtomicInteger mOpenCounter = new AtomicInteger();

    private static BrelokDatabaseManager instance;
    private static SQLiteOpenHelper mDatabaseHelper;
    private SQLiteDatabase mDatabase;

    public static synchronized void initializeInstance(SQLiteOpenHelper helper) {
        if (instance == null) {
            instance = new BrelokDatabaseManager();
            mDatabaseHelper = helper;
        }
    }

    public static synchronized BrelokDatabaseManager getInstance() {
        if (instance == null) {
            throw new IllegalStateException(BrelokDatabaseManager.class.getSimpleName() +
                    " is not initialized, call initializeInstance(..) method first.");
        }

        return instance;
    }

    public SQLiteDatabase openDatabase() {
        if(mOpenCounter.incrementAndGet() == 1) {
            // Opening new database
            mDatabase = mDatabaseHelper.getWritableDatabase();
        }
        return mDatabase;
    }

    public void closeDatabase() {
        if(mOpenCounter.decrementAndGet() == 0) {
            // Closing database
            mDatabase.close();

        }
    }

}
