package com.example.BrelokAndroid.AsyncTasks;

import android.os.AsyncTask;
import com.example.BrelokAndroid.BaseMobile.BaseMobile;
import com.example.BrelokAndroid.BaseMobile.LoginAnswer;
import com.example.BrelokAndroid.BaseMobile.VectorGisPoint;
import com.example.BrelokAndroid.BrelokDataStructures.BrelokMyLocation;

/**
 * Created with IntelliJ IDEA.
 * User: Alexey
 * Date: 03.11.13
 * Time: 0:38
 * To change this template use File | Settings | File Templates.
 */
//TODO добвить isonShip
public class SendSelfPointsTask extends AsyncTask<BrelokMyLocation,Void,LoginAnswer> {
    @Override
    protected LoginAnswer doInBackground(BrelokMyLocation... params) {
        BaseMobile bm = new BaseMobile();
        return bm.SetSelfPoints(params[0].gisPoints,params[0].sessionId,params[0].currentTime,true,params[0].isOnShip,true);
    }
}
