package com.example.BrelokAndroid.AsyncTasks;

import android.os.AsyncTask;
import com.example.BrelokAndroid.BaseMobile.BaseMobile;
import com.example.BrelokAndroid.BaseMobile.LoginAnswer;
import com.example.BrelokAndroid.BrelokDataStructures.BrelokUserInfo;

/**
 * Created with IntelliJ IDEA.
 * User: Admin
 * Date: 25.10.13
 * Time: 14:58
 * To change this template use File | Settings | File Templates.
 */

/***
 * acynctask для пинка сервиса на предмет жив или нет. На вход сессия, на выход приходит инфа о юзере.
 */
public class CheckStateTask extends AsyncTask<BrelokUserInfo,Void, LoginAnswer> {
    @Override
    protected LoginAnswer doInBackground(BrelokUserInfo... userInfos) {
        BaseMobile bm=new BaseMobile();
        return bm.CheckState(userInfos[0].sessionId);
    }
}
