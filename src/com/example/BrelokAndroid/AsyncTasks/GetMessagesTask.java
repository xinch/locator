package com.example.BrelokAndroid.AsyncTasks;

import android.os.AsyncTask;
import com.example.BrelokAndroid.BaseMobile.BaseMobile;
import com.example.BrelokAndroid.BaseMobile.GetMessageAnswer;
import com.example.BrelokAndroid.BrelokDataStructures.BrelokGetMessage;

/**
 * Created with IntelliJ IDEA.
 * User: Admin
 * Date: 25.10.13
 * Time: 15:26
 * To change this template use File | Settings | File Templates.
 */

public class GetMessagesTask extends AsyncTask<BrelokGetMessage,Void,GetMessageAnswer> {
    @Override
    protected GetMessageAnswer doInBackground(BrelokGetMessage... params) {
        BaseMobile bm=new BaseMobile();
        return bm.GetMessages(params[0].fromDate,true,params[0].SessionId);  //To change body of implemented methods use File | Settings | File Templates.
    }
}
