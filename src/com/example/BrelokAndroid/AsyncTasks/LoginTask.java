package com.example.BrelokAndroid.AsyncTasks;

import android.os.AsyncTask;
import com.example.BrelokAndroid.BaseMobile.BaseMobile;
import com.example.BrelokAndroid.BaseMobile.LoginAnswer;
import com.example.BrelokAndroid.BrelokDataStructures.BrelokUserInfo;

/**
 * Created with IntelliJ IDEA.
 * User: Admin
 * Date: 25.10.13
 * Time: 14:00
 * To change this template use File | Settings | File Templates.
 */

/***
 * asyncrask для логиа. На вход приходит логин пароль и сессия, если логин после разлогина или новый юзер
 */
public class LoginTask extends AsyncTask<BrelokUserInfo,Void,LoginAnswer> {
    @Override
    protected LoginAnswer doInBackground(BrelokUserInfo... userInfos) {
        BaseMobile bm=new BaseMobile();
        return bm.Login(userInfos[0].login, userInfos[0].pass, userInfos[0].sessionId);
    }
}
