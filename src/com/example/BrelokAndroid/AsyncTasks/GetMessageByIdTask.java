package com.example.BrelokAndroid.AsyncTasks;

import android.os.AsyncTask;
import com.example.BrelokAndroid.BaseMobile.BaseMobile;
import com.example.BrelokAndroid.BaseMobile.GetMessageAnswer;
import com.example.BrelokAndroid.BaseMobile.VectorInt64;
import com.example.BrelokAndroid.BrelokDataStructures.BrelokGetMessagesByID;
import com.example.BrelokAndroid.BrelokDbAdapter;

/**
 * Created with IntelliJ IDEA.
 * User: Admin
 * Date: 25.10.13
 * Time: 15:27
 * To change this template use File | Settings | File Templates.
 */
/***
 * отправка списка ид для потверждения приема
 */
public class GetMessageByIdTask extends AsyncTask<BrelokGetMessagesByID,Void,GetMessageAnswer> {
    @Override
    protected GetMessageAnswer doInBackground(BrelokGetMessagesByID... params) {
        BaseMobile bm=new BaseMobile();
        return bm.GetMessagesById(params[0].Ids,params[0].SessionId);
    }
}
