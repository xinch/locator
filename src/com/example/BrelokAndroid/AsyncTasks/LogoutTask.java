package com.example.BrelokAndroid.AsyncTasks;

import android.os.AsyncTask;
import com.example.BrelokAndroid.BaseMobile.BaseMobile;
import com.example.BrelokAndroid.BaseMobile.LoginAnswer;
import com.example.BrelokAndroid.BrelokDataStructures.BrelokUserInfo;

/**
 * Created with IntelliJ IDEA.
 * User: Admin
 * Date: 25.10.13
 * Time: 14:14
 * To change this template use File | Settings | File Templates.
 */

/***
 * asynctask для разлогина. На вход прихоодит текущий залогиненый юзер. Подразумевается, что залогинется может только 1 за раз
 */
public class LogoutTask extends AsyncTask<BrelokUserInfo,Void,LoginAnswer>{
    @Override
    protected LoginAnswer doInBackground(BrelokUserInfo... userInfos) {
        BaseMobile bm=new BaseMobile();
        return bm.Logout(userInfos[0].sessionId);  //To change body of implemented methods use File | Settings | File Templates.
    }
}