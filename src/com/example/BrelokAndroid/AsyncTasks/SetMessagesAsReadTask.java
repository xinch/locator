package com.example.BrelokAndroid.AsyncTasks;

import android.os.AsyncTask;
import com.example.BrelokAndroid.BaseMobile.BaseMobile;
import com.example.BrelokAndroid.BaseMobile.SetMessageReadAnswer;
import com.example.BrelokAndroid.BrelokDataStructures.BrelokGetMessage;
import com.example.BrelokAndroid.BrelokDataStructures.BrelokSendReadedMsgId;

/**
 * Created by Admin on 12.12.13.
 */
public class SetMessagesAsReadTask extends AsyncTask<BrelokSendReadedMsgId,Void,SetMessageReadAnswer> {
    @Override
    protected SetMessageReadAnswer doInBackground(BrelokSendReadedMsgId... params) {
        BaseMobile baseMobile = new BaseMobile();
        return baseMobile.SetMessageReadAsRead(params[0].ids,params[0].SessionId);
    }
}
