package com.example.BrelokAndroid;


import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.TextView;
import com.example.BrelokAndroid.BrelokConverters.BrelokDateConvertor;
import com.example.BrelokAndroid.BrelokDataStructures.BrelokMsgLocation;
import com.example.BrelokAndroid.DBTables.BrelokCoordinatesReceived;
import com.example.BrelokAndroid.DBTables.BrelokMessagesArchive;
import com.google.android.gms.maps.*;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

/**
 * Created with IntelliJ IDEA.
 * User: Alexey
 * Date: 02.11.13
 * Time: 23:58
 * To change this template use File | Settings | File Templates.
 */
public class BrelokMessageScreen extends FragmentActivity {

    private GoogleMap map;
    private BrelokDbAdapter dbAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.message_screen);

        int id = getIntent().getIntExtra("id",0);

        TextView textViewMessageNumb = (TextView) findViewById(R.id.txtMessageId);
        TextView textViewCreateDate = (TextView) findViewById(R.id.txtCrealeData);
        TextView textViewMsg = (TextView) findViewById(R.id.txtMessage);

        BrelokDbAdapter dbAdapter = new BrelokDbAdapter(getApplicationContext());
        BrelokMsgLocation bml;
        dbAdapter.openHelper();
        Cursor cursor = dbAdapter.getMessageById(id);
        if (cursor!=null){
            if(cursor.moveToFirst()){
                textViewMessageNumb.setText(""+cursor.getInt(cursor.getColumnIndex(BrelokMessagesArchive.colMessageID)));
                BrelokDateConvertor bdc = new BrelokDateConvertor();

                textViewCreateDate.setText(""+bdc.convertToShow(cursor.getString(cursor.getColumnIndex(BrelokMessagesArchive.colCreateTime))));
                 //TODO обрезать строки, чтобы влезали в экран
                String [] words =  cursor.getString(cursor.getColumnIndex(BrelokMessagesArchive.colText)).split(" ");

                textViewMsg.setText(""+cursor.getString(cursor.getColumnIndex(BrelokMessagesArchive.colText)));
                if(cursor.getInt(cursor.getColumnIndex(BrelokMessagesArchive.colCoordinates))!=0){
                    bml = new BrelokMsgLocation(cursor.getDouble(cursor.getColumnIndex(BrelokCoordinatesReceived.colLatitude))
                            ,cursor.getDouble(cursor.getColumnIndex(BrelokCoordinatesReceived.colLongitude)));
                    FrameLayout frameLayout = (FrameLayout) findViewById(R.id.FLMap);
                    frameLayout.setVisibility(View.VISIBLE);

                    android.support.v4.app.FragmentManager myFragmentManager = getSupportFragmentManager();
                    SupportMapFragment mySupportMapFragment = (SupportMapFragment) myFragmentManager
                            .findFragmentById(R.id.map_message);

                    map = mySupportMapFragment.getMap();

                    addMarker(bml,false);
                }
            }
        }
        dbAdapter.closeHelper();
    }

    private void addMarker(BrelokMsgLocation location, boolean ifCur) {
        if(location!=null){
            LatLng curPos=new LatLng(location.Latitude,location.Longitude);
            if (ifCur){
                map.addMarker(new MarkerOptions().position(curPos).title("Текущая позиция").icon(BitmapDescriptorFactory.fromResource(R.drawable.arrow)));
            }   else {
                map.addMarker(new MarkerOptions().position(curPos).icon(BitmapDescriptorFactory.fromResource(R.drawable.msg_marker)));
            }

                CameraUpdate center= CameraUpdateFactory.newLatLngZoom(curPos, 16);
                map.animateCamera(center);
            }
        }
}
