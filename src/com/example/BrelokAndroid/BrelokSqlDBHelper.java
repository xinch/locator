package com.example.BrelokAndroid;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import com.example.BrelokAndroid.DBTables.*;

/**
 * Created with IntelliJ IDEA.
 * User: Admin
 * Date: 27.09.13
 * Time: 15:17
 * To change this template use File | Settings | File Templates.
 */
public class BrelokSqlDBHelper extends SQLiteOpenHelper implements BrelokCoordinatesArchive,
        BrelokAuthorisations,BrelokCoordinatesReceived,BrelokMessagesArchive, BrelokQuerry, BrelokSettings {

    private static final String DATBASE_NAME="BrelokMobileData";
    private static final int DATABASE_VERSION=21;

    private static final String DROP_TABLE_QUERRY="DROP TABLE IF EXISTS ";

    public BrelokSqlDBHelper(Context context){
        super(context, DATBASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE "+BrelokCoordinatesReceived.coordinatesReceived+"( "+BrelokCoordinatesReceived.colLocationID+" INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, "
                +BrelokCoordinatesReceived.colLatitude+" REAL NOT NULL, "
                +BrelokCoordinatesReceived.colLongitude+" REAL NOT NULL, "
                +BrelokCoordinatesReceived.colAltitude+" REAL)");

        db.execSQL("CREATE TABLE "+ BrelokCoordinatesArchive.coordinatesArchive + "( "+BrelokCoordinatesArchive.colLocationID+" INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, "
                +BrelokCoordinatesArchive.colLongitude+" REAL NOT NULL, "
                +BrelokCoordinatesArchive.colLatitude+" REAL NOT NULL, "
                +BrelokCoordinatesArchive.colAltitude+" REAL NOT NULL, "
                +BrelokCoordinatesArchive.colAccurancy+" REAL NOT NULL, "
                +BrelokCoordinatesArchive.colBearing+" REAL NOT NULL, "
                +BrelokCoordinatesArchive.colSpeed+" REAL NOT NULL, "
                +BrelokCoordinatesArchive.colDateTtime+" STRING NOT NULL )");

                //TODO переделать таблицы  BrelokMessagesArchive и BrelokCoordinatesReceived. Сделать ссылку на _id сообщения из таблицы координат, дабы скоратить количество null в результаха выборки

        db.execSQL("CREATE TABLE " + BrelokMessagesArchive.messagesArchive + "( " + BrelokMessagesArchive.colMessageID + " INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, "
                + BrelokMessagesArchive.colText + " TEXT NOT NULL,  "
                + BrelokMessagesArchive.colExternalID + " INTEGER, "
                + BrelokMessagesArchive.colReaded + " INTEGER NOT NULL, "
                + BrelokMessagesArchive.colWarningLevel + " INTEGER, "
                + BrelokMessagesArchive.colCreateTime + " STRING NOT NULL, "
                + BrelokMessagesArchive.colCoordinates + " INTEGER, FOREIGN KEY( " + BrelokMessagesArchive.colCoordinates + " ) REFERENCES " + BrelokCoordinatesReceived.coordinatesReceived + "( " + BrelokCoordinatesReceived.colLocationID + " ))");

        db.execSQL("CREATE TABLE "+BrelokAuthorisations.authorisations +"( "+BrelokAuthorisations.colID+" INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, "
                +BrelokAuthorisations.colLogin+" TEXT , "
                +BrelokAuthorisations.colPassword+" TEXT , "
                +BrelokAuthorisations.colSessionId+" TEXT, "
                +BrelokAuthorisations.colIsOnAShip+ " INTEGER,"
                +BrelokAuthorisations.colIsLogined+ " INTEGER )");

        db.execSQL("CREATE TABLE "+BrelokQuerry.querry+"( "+BrelokQuerry.colID+" INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, "
                +BrelokQuerry.colTableName+" TEXT , "
                +BrelokQuerry.colIdToSend+" INTEGER )");

        db.execSQL("CREATE TABLE "+ BrelokSettings.settings+"( "+ BrelokSettings.colID+" INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, "
                + BrelokSettings.colServiceUrl+" TEXT , "
                + BrelokSettings.colServiceTimeout+" INTEGER , "
                + BrelokSettings.colGpsAccuracy+" INTEGER , "
                + BrelokSettings.colNETWORKAccuracy+" INTEGER , "
                + BrelokSettings.colGetCoordinatesTimeout+" INTEGER , "
                + BrelokSettings.colServiceExchangeTimeout+" INTEGER )");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL(DROP_TABLE_QUERRY+BrelokMessagesArchive.messagesArchive);
        db.execSQL(DROP_TABLE_QUERRY+BrelokCoordinatesArchive.coordinatesArchive);
        db.execSQL(DROP_TABLE_QUERRY+BrelokCoordinatesReceived.coordinatesReceived);
        db.execSQL(DROP_TABLE_QUERRY+BrelokAuthorisations.authorisations);
        db.execSQL(DROP_TABLE_QUERRY+BrelokQuerry.querry);
        db.execSQL(DROP_TABLE_QUERRY+ BrelokSettings.settings);

        onCreate(db);
    }
}
