package com.example.BrelokAndroid;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;
import com.example.BrelokAndroid.BaseMobile.*;
import com.example.BrelokAndroid.BrelokDataStructures.BrelokUserInfo;
import com.example.BrelokAndroid.AsyncTasks.LoginTask;
import java.util.concurrent.ExecutionException;


/**
 * Created with IntelliJ IDEA.
 * User: Admin
 * Date: 14.10.13
 * Time: 11:38
 * To change this template use File | Settings | File Templates.
 */
public class BrelokLogin extends Activity {

    BrelokDbAdapter dbAdapter;
    BrelokUserInfo userInfo;
    EditText username, password;
    CheckBox onBoard;
    Button btnLogin;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login_screen);
        onBoard = (CheckBox)findViewById(R.id.chkBoxOnBoard);
        username = (EditText)findViewById(R.id.txtUserName);
        password = (EditText)findViewById(R.id.txtPassword);
        btnLogin = (Button)findViewById(R.id.login);
        btnLogin.setOnClickListener(loginListener);

        dbAdapter=new BrelokDbAdapter(getApplicationContext());

        dbAdapter.openHelper();
        boolean isLogined=dbAdapter.ifUserLoggined();
        dbAdapter.closeHelper();
        if(isLogined){  // если уже логинились, то незачем запускать окно логина еще раз
            Intent intent=new Intent(getApplicationContext(), BrelokMainActivity.class);
            startActivity(intent);
            finish();
        }
    }

    //TODO переписать без кучи одинаковых обьектов
    private View.OnClickListener loginListener = new View.OnClickListener() {
        public void onClick(View v) {
            userInfo=new BrelokUserInfo(username.getText().toString(),password.getText().toString(),"");
                doLogin();
    };
    };

    private void doLogin(){
        dbAdapter.openHelper();
        userInfo.sessionId=dbAdapter.getSessionID(userInfo);
        dbAdapter.closeHelper();
        // TODO переделать через генерируемые stubAsunc
        LoginAnswer login;
        LoginTask t=new LoginTask();    // сделал через async
        t.execute(userInfo);
        try { //TODO уже через 2 дня понять не могу что здесь написано. Написать коментов
            login = t.get();
            if(login!=null){
            if(login.isLoggedIn && login!=null){
                Toast.makeText(getApplicationContext(), getString(R.string.msgSuccessLogin), Toast.LENGTH_LONG).show();
                dbAdapter.openHelper();
                if (!dbAdapter.isUserExist(userInfo)){
                    if (userInfo.sessionId.isEmpty()){
                        userInfo.sessionId=login.sessionId;
                    }
                    dbAdapter.insertUser(userInfo);
                }
                if (userInfo.sessionId.isEmpty()){
                    userInfo.sessionId=login.sessionId;
                    dbAdapter.writeSessionId(userInfo);
                }
                dbAdapter.setLogedIn(userInfo,onBoard.isChecked());
                dbAdapter.closeHelper();
                Intent intent=new Intent(getApplicationContext(), BrelokMainActivity.class);
                startActivity(intent);
                finish(); // убивается окно login
            }else
                Toast.makeText(getApplicationContext(), "Введены неверные имя пользователя/пароль", Toast.LENGTH_LONG).show();
            }
        } catch (InterruptedException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        } catch (ExecutionException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }finally {
        }
    }
}
